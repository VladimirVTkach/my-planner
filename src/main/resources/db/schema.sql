create table if not exists user_account
(
    user_id    bigint generated always as identity,
    login      varchar(25) not null,
    password   varchar(70) not null,
    first_name varchar(30) not null,
    constraint pk_user_account
        primary key (user_id),
    constraint uq_user_account_login
        unique (login)
);

create table if not exists project
(
    project_id bigint generated always as identity (maxvalue 2147483647),
    name       varchar(30) not null,
    goal       varchar(255),
    priority   integer     not null,
    creation_time timestamp not null,
    update_time timestamp,
    user_id    bigint      not null,
    constraint pk_project
        primary key (project_id),
    constraint fk_project_user_account
        foreign key (user_id) references user_account
            on delete cascade,
    constraint uq_project_user_id_name
        unique (user_id, name),
    constraint ck_project_priority
        check (priority > 0)
);

create table if not exists task
(
    task_id             bigint generated always as identity (maxvalue 2147483647),
    project_id          bigint      not null,
    title               varchar(40) not null,
    description         varchar(255),
    deadline            timestamp   not null,
    priority            integer     not null,
    time_for_completion bigint      not null,
    time_for_rest       bigint      not null,
    creation_time       timestamp   not null,
    update_time         timestamp,
    completed           boolean     not null,
    constraint pk_task
        primary key (task_id),
    constraint fk_task_project
        foreign key (project_id) references project
            on delete cascade,
    constraint ck_task_priority
        check (priority > 0)
);

create table if not exists working_session
(
    working_session_id bigint generated always as identity (maxvalue 2147483647),
    user_id            bigint      not null,
    start_time         timestamp,
    specified_duration bigint      not null,
    actual_duration    bigint      not null,
    state              varchar(10) not null,
    constraint pk_working_session
        primary key (working_session_id),
    constraint fk_working_session_user
        foreign key (user_id) references user_account
            on delete cascade
);

create table if not exists unit_of_work
(
    unit_of_work_id    bigint generated always as identity (maxvalue 2147483647),
    working_session_id bigint not null,
    task_id            bigint not null,
    start_time         timestamp,
    end_time           timestamp,
    constraint pk_unit_of_work
        primary key (unit_of_work_id),
    constraint fk_unit_of_work_working_session
        foreign key (working_session_id) references working_session
            on delete cascade,
    constraint fk_unit_of_work_task
        foreign key (task_id) references task
            on delete restrict
);


