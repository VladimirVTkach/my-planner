package com.vladimir.tkach.myplanner.application;

import com.vladimir.tkach.myplanner.application.dto.*;
import com.vladimir.tkach.myplanner.application.exception.EntityNotFoundException;
import com.vladimir.tkach.myplanner.domain.planning.Task;
import com.vladimir.tkach.myplanner.domain.user.Project;
import com.vladimir.tkach.myplanner.domain.user.User;
import com.vladimir.tkach.myplanner.infrastructure.repository.ProjectRepository;
import com.vladimir.tkach.myplanner.infrastructure.repository.TaskRepository;
import com.vladimir.tkach.myplanner.infrastructure.repository.UserRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import javax.validation.ConstraintViolationException;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
public class TaskServiceTest {
    private final TaskService taskService;
    private final ProjectRepository projectRepository;
    private final TaskRepository taskRepository;
    private final UserRepository userRepository;

    @Autowired
    public TaskServiceTest(TaskService taskService,
                           ProjectRepository projectRepository,
                           TaskRepository taskRepository,
                           UserRepository userRepository) {
        this.taskService = taskService;
        this.projectRepository = projectRepository;
        this.taskRepository = taskRepository;
        this.userRepository = userRepository;
    }

    @BeforeEach
    public void cleanUp() {
        taskRepository.deleteAll();
        projectRepository.deleteAll();
        userRepository.deleteAll();
    }

    @Test
    public void testGetTaskById_whenTaskWithPassedIdDoesNotExist() {
        assertThrows(EntityNotFoundException.class, () -> taskService.getTaskById(GetTaskByIdRequest.builder()
                .userLogin("user_1")
                .taskId(1L)
                .build()));
    }

    @Test
    public void testGetTaskById_whenOwnerOfProjectWhichContainsTaskIsOtherUser() {
        User user = User.builder()
                .login("user_1")
                .password("password")
                .firstName("first_name")
                .build();

        userRepository.save(user);

        Project project = Project.builder()
                .name("test_project")
                .goal("test_goal")
                .priority(1)
                .owner(user)
                .build();

        projectRepository.save(project);

        Task task = Task.builder()
                .project(project)
                .title("test_task")
                .description("description")
                .deadline(LocalDateTime.now().plus(Duration.ofMinutes(30)))
                .priority(1)
                .timeForCompletion(Duration.ofMinutes(15))
                .timeForRest(Duration.ofMinutes(5))
                .build();

        Task savedTask = taskRepository.save(task);

        assertThrows(EntityNotFoundException.class, () -> taskService.getTaskById(GetTaskByIdRequest.builder()
                .userLogin("user_2")
                .taskId(savedTask.getTaskId())
                .build()));
    }

    @Test
    public void testGetTaskById_whenEverythingIsOk() {
        User user = User.builder()
                .login("user_1")
                .password("password")
                .firstName("first_name")
                .build();

        userRepository.save(user);

        Project project = Project.builder()
                .name("test_project")
                .goal("test_goal")
                .priority(1)
                .owner(user)
                .build();

        projectRepository.save(project);

        Task task = Task.builder()
                .project(project)
                .title("test_task")
                .description("description")
                .deadline(LocalDateTime.now().plus(Duration.ofMinutes(30)))
                .priority(1)
                .timeForCompletion(Duration.ofMinutes(15))
                .timeForRest(Duration.ofMinutes(5))
                .build();

        Task savedTask = taskRepository.save(task);

        TaskInfo taskInfo = taskService.getTaskById(GetTaskByIdRequest.builder()
                .userLogin(user.getLogin())
                .taskId(savedTask.getTaskId())
                .build());

        assertEquals(task.getProject().getProjectId(), taskInfo.getProjectId());
        assertEquals(task.getTitle(), taskInfo.getTitle());
        assertEquals(task.getDescription(), taskInfo.getDescription());
        assertEquals(task.getDeadline(), taskInfo.getDeadline());
        assertEquals(task.getPriority(), taskInfo.getPriority());
        assertEquals(task.getTimeForCompletion(), taskInfo.getTimeForCompletion());
        assertEquals(task.getTimeForRest(), taskInfo.getTimeForRest());
        assertEquals(task.getCreationTime(), taskInfo.getCreationTime());
        assertEquals(task.getUpdateTime(), taskInfo.getUpdateTime());
        assertEquals(task.isCompleted(), taskInfo.isCompleted());
    }

    @Test
    public void testGetProjectTasks_whenProjectWithPassedIdDoesntExist() {
        User user = User.builder()
                .login("user_1")
                .password("password")
                .firstName("first_name")
                .build();

        userRepository.save(user);

        assertThrows(EntityNotFoundException.class, () -> taskService.getProjectTasks(GetProjectTasksRequest.builder()
                .userLogin(user.getLogin())
                .projectId(1L)
                .build()));
    }

    @Test
    public void testGetProjectTasks_whenOwnerOfPassedProjectIsOtherUser() {
        User user = User.builder()
                .login("user_1")
                .password("password")
                .firstName("first_name")
                .build();

        userRepository.save(user);

        Project project = Project.builder()
                .name("test_project_1")
                .goal("test_goal")
                .priority(1)
                .owner(user)
                .build();

        Project savedProject = projectRepository.save(project);

        assertThrows(EntityNotFoundException.class, () -> taskService.getProjectTasks(GetProjectTasksRequest.builder()
                .userLogin("user_2")
                .projectId(savedProject.getProjectId())
                .build()));
    }

    @Test
    public void testGetProjectTasks_whenLimitIsPassedAndInvalid() {
        User user = User.builder()
                .login("user_1")
                .password("password")
                .firstName("first_name")
                .build();

        userRepository.save(user);

        Project firstProject = Project.builder()
                .name("test_project_1")
                .goal("test_goal")
                .priority(1)
                .owner(user)
                .build();

        Project savedFirstProject = projectRepository.save(firstProject);

        Project secondProject = Project.builder()
                .name("test_project_2")
                .goal("test_goal")
                .priority(3)
                .owner(user)
                .build();

        projectRepository.save(secondProject);

        Task firstTask = Task.builder()
                .project(firstProject)
                .title("test_task_1")
                .description("test_description_1")
                .deadline(LocalDateTime.now().plus(Duration.ofMinutes(30)))
                .priority(1)
                .timeForCompletion(Duration.ofMinutes(15))
                .timeForRest(Duration.ofMinutes(5))
                .build();

        taskRepository.save(firstTask);

        Task secondTask = Task.builder()
                .project(secondProject)
                .title("test_task_2")
                .description("test_description_2")
                .deadline(LocalDateTime.now().plus(Duration.ofMinutes(25)))
                .priority(5)
                .timeForCompletion(Duration.ofMinutes(5))
                .timeForRest(Duration.ofMinutes(0))
                .build();

        taskRepository.save(secondTask);

        assertThrows(ConstraintViolationException.class, () -> taskService.getProjectTasks(GetProjectTasksRequest.builder()
                .userLogin(user.getLogin())
                .projectId(savedFirstProject.getProjectId())
                .limit(-1)
                .build()));
    }

    @Test
    public void testGetProjectTasks_whenOffsetIsPassedAndInvalid() {
        User user = User.builder()
                .login("user_1")
                .password("password")
                .firstName("first_name")
                .build();

        userRepository.save(user);

        Project firstProject = Project.builder()
                .name("test_project_1")
                .goal("test_goal")
                .priority(1)
                .owner(user)
                .build();

        Project savedFirstProject = projectRepository.save(firstProject);

        Project secondProject = Project.builder()
                .name("test_project_2")
                .goal("test_goal")
                .priority(3)
                .owner(user)
                .build();

        projectRepository.save(secondProject);

        Task firstTask = Task.builder()
                .project(firstProject)
                .title("test_task_1")
                .description("test_description_1")
                .deadline(LocalDateTime.now().plus(Duration.ofMinutes(30)))
                .priority(1)
                .timeForCompletion(Duration.ofMinutes(15))
                .timeForRest(Duration.ofMinutes(5))
                .build();

        taskRepository.save(firstTask);

        Task secondTask = Task.builder()
                .project(secondProject)
                .title("test_task_2")
                .description("test_description_2")
                .deadline(LocalDateTime.now().plus(Duration.ofMinutes(25)))
                .priority(5)
                .timeForCompletion(Duration.ofMinutes(5))
                .timeForRest(Duration.ofMinutes(0))
                .build();

        taskRepository.save(secondTask);

        assertThrows(ConstraintViolationException.class, () -> taskService.getProjectTasks(GetProjectTasksRequest.builder()
                .userLogin(user.getLogin())
                .projectId(savedFirstProject.getProjectId())
                .offset(-1)
                .build()));
    }

    @Test
    public void testGetProjectTasks_whenOnlyProjectIdIsPassed() {
        User user = User.builder()
                .login("user_1")
                .password("password")
                .firstName("first_name")
                .build();

        userRepository.save(user);

        Project firstProject = Project.builder()
                .name("test_project_1")
                .goal("test_goal")
                .priority(1)
                .owner(user)
                .build();

        Project savedFirstProject = projectRepository.save(firstProject);

        Project secondProject = Project.builder()
                .name("test_project_2")
                .goal("test_goal")
                .priority(3)
                .owner(user)
                .build();

        projectRepository.save(secondProject);

        Task firstTask = Task.builder()
                .project(firstProject)
                .title("test_task_1")
                .description("test_description_1")
                .deadline(LocalDateTime.now().plus(Duration.ofMinutes(30)))
                .priority(1)
                .timeForCompletion(Duration.ofMinutes(15))
                .timeForRest(Duration.ofMinutes(5))
                .build();

        Task savedFirstTask = taskRepository.save(firstTask);

        Task secondTask = Task.builder()
                .project(secondProject)
                .title("test_task_2")
                .description("test_description_2")
                .deadline(LocalDateTime.now().plus(Duration.ofMinutes(25)))
                .priority(5)
                .timeForCompletion(Duration.ofMinutes(5))
                .timeForRest(Duration.ofMinutes(0))
                .build();

        taskRepository.save(secondTask);

        List<TaskInfo> projectTasks = taskService.getProjectTasks(GetProjectTasksRequest.builder()
                .userLogin(user.getLogin())
                .projectId(savedFirstProject.getProjectId())
                .build());

        assertEquals(1, projectTasks.size());
        assertTrue(projectTasks.contains(taskService.getTaskById(GetTaskByIdRequest.builder()
                .userLogin(user.getLogin())
                .taskId(savedFirstTask.getTaskId())
                .build())
        ));
    }

    @Test
    public void testGetProjectTasks_whenTitleIsPassed() {
        User user = User.builder()
                .login("user_1")
                .password("password")
                .firstName("first_name")
                .build();

        userRepository.save(user);

        Project firstProject = Project.builder()
                .name("test_project_1")
                .goal("test_goal")
                .priority(1)
                .owner(user)
                .build();

        Project savedFirstProject = projectRepository.save(firstProject);

        Project secondProject = Project.builder()
                .name("test_project_2")
                .goal("test_goal")
                .priority(3)
                .owner(user)
                .build();

        projectRepository.save(secondProject);

        Task firstTask = Task.builder()
                .project(firstProject)
                .title("test_task_1")
                .description("test_description_1")
                .deadline(LocalDateTime.now().plus(Duration.ofMinutes(30)))
                .priority(1)
                .timeForCompletion(Duration.ofMinutes(15))
                .timeForRest(Duration.ofMinutes(5))
                .build();

        Task savedFirstTask = taskRepository.save(firstTask);

        Task secondTask = Task.builder()
                .project(firstProject)
                .title("test_task_2")
                .description("test_description_2")
                .deadline(LocalDateTime.now().plus(Duration.ofMinutes(25)))
                .priority(5)
                .timeForCompletion(Duration.ofMinutes(5))
                .timeForRest(Duration.ofMinutes(0))
                .build();

        taskRepository.save(secondTask);

        Task thirdTask = Task.builder()
                .project(secondProject)
                .title("test_task_3")
                .description("test_description_3")
                .deadline(LocalDateTime.now().plus(Duration.ofMinutes(50)))
                .priority(1)
                .timeForCompletion(Duration.ofMinutes(20))
                .timeForRest(Duration.ofMinutes(5))
                .build();

        taskRepository.save(thirdTask);

        List<TaskInfo> projectTasks = taskService.getProjectTasks(GetProjectTasksRequest.builder()
                .userLogin(user.getLogin())
                .projectId(savedFirstProject.getProjectId())
                .title(savedFirstTask.getTitle())
                .build());

        assertEquals(1, projectTasks.size());
        assertTrue(projectTasks.contains(taskService.getTaskById(GetTaskByIdRequest.builder()
                .userLogin(user.getLogin())
                .taskId(savedFirstTask.getTaskId())
                .build())
        ));
    }

    @Test
    public void testGetProjectTasks_whenLimitAndOffsetArePassedAndValid() {
        User user = User.builder()
                .login("user_1")
                .password("password")
                .firstName("first_name")
                .build();

        userRepository.save(user);

        Project firstProject = Project.builder()
                .name("test_project_1")
                .goal("test_goal")
                .priority(1)
                .owner(user)
                .build();

        Project savedFirstProject = projectRepository.save(firstProject);

        Project secondProject = Project.builder()
                .name("test_project_2")
                .goal("test_goal")
                .priority(3)
                .owner(user)
                .build();

        projectRepository.save(secondProject);

        Task firstTask = Task.builder()
                .project(firstProject)
                .title("test_task_1")
                .description("test_description_1")
                .deadline(LocalDateTime.now().plus(Duration.ofMinutes(30)))
                .priority(1)
                .timeForCompletion(Duration.ofMinutes(15))
                .timeForRest(Duration.ofMinutes(5))
                .build();

        Task savedFirstTask = taskRepository.save(firstTask);

        Task secondTask = Task.builder()
                .project(firstProject)
                .title("test_task_2")
                .description("test_description_2")
                .deadline(LocalDateTime.now().plus(Duration.ofMinutes(25)))
                .priority(5)
                .timeForCompletion(Duration.ofMinutes(5))
                .timeForRest(Duration.ofMinutes(0))
                .build();

        Task savedSecondTask = taskRepository.save(secondTask);

        Task thirdTask = Task.builder()
                .project(secondProject)
                .title("test_task_3")
                .description("test_description_3")
                .deadline(LocalDateTime.now().plus(Duration.ofMinutes(50)))
                .priority(1)
                .timeForCompletion(Duration.ofMinutes(20))
                .timeForRest(Duration.ofMinutes(5))
                .build();

        taskRepository.save(thirdTask);

        List<TaskInfo> projectTasks = taskService.getProjectTasks(GetProjectTasksRequest.builder()
                .userLogin(user.getLogin())
                .projectId(savedFirstProject.getProjectId())
                .limit(1)
                .offset(0)
                .build());

        assertEquals(1, projectTasks.size());
        assertTrue(projectTasks.contains(taskService.getTaskById(GetTaskByIdRequest.builder()
                .userLogin(user.getLogin())
                .taskId(savedFirstTask.getTaskId())
                .build())) ||
                projectTasks.contains(taskService.getTaskById(GetTaskByIdRequest.builder()
                        .userLogin(user.getLogin())
                        .taskId(savedSecondTask.getTaskId())
                        .build())
                ));
    }

    @Test
    public void testGetAllTasks_whenLimitIsPassedAndInvalid() {
        User user = User.builder()
                .login("user_1")
                .password("password")
                .firstName("first_name")
                .build();

        userRepository.save(user);

        Project firstProject = Project.builder()
                .name("test_project_1")
                .goal("test_goal")
                .priority(1)
                .owner(user)
                .build();

        projectRepository.save(firstProject);

        Project secondProject = Project.builder()
                .name("test_project_2")
                .goal("test_goal")
                .priority(3)
                .owner(user)
                .build();

        projectRepository.save(secondProject);

        Task firstTask = Task.builder()
                .project(firstProject)
                .title("test_task_1")
                .description("test_description_1")
                .deadline(LocalDateTime.now().plus(Duration.ofMinutes(30)))
                .priority(1)
                .timeForCompletion(Duration.ofMinutes(15))
                .timeForRest(Duration.ofMinutes(5))
                .build();

        taskRepository.save(firstTask);

        Task secondTask = Task.builder()
                .project(secondProject)
                .title("test_task_2")
                .description("test_description_2")
                .deadline(LocalDateTime.now().plus(Duration.ofMinutes(25)))
                .priority(5)
                .timeForCompletion(Duration.ofMinutes(5))
                .timeForRest(Duration.ofMinutes(0))
                .build();

        taskRepository.save(secondTask);

        assertThrows(ConstraintViolationException.class, () -> taskService.getAllTasks(GetAllTasksRequest.builder()
                .userLogin(user.getLogin())
                .limit(-5)
                .offset(0)
                .build()));
    }

    @Test
    public void testGetAllTasks_whenOffsetIsPassedAndInvalid() {
        User user = User.builder()
                .login("user_1")
                .password("password")
                .firstName("first_name")
                .build();

        userRepository.save(user);

        Project firstProject = Project.builder()
                .name("test_project_1")
                .goal("test_goal")
                .priority(1)
                .owner(user)
                .build();

        projectRepository.save(firstProject);

        Project secondProject = Project.builder()
                .name("test_project_2")
                .goal("test_goal")
                .priority(3)
                .owner(user)
                .build();

        projectRepository.save(secondProject);

        Task firstTask = Task.builder()
                .project(firstProject)
                .title("test_task_1")
                .description("test_description_1")
                .deadline(LocalDateTime.now().plus(Duration.ofMinutes(30)))
                .priority(1)
                .timeForCompletion(Duration.ofMinutes(15))
                .timeForRest(Duration.ofMinutes(5))
                .build();

        taskRepository.save(firstTask);

        Task secondTask = Task.builder()
                .project(secondProject)
                .title("test_task_2")
                .description("test_description_2")
                .deadline(LocalDateTime.now().plus(Duration.ofMinutes(25)))
                .priority(5)
                .timeForCompletion(Duration.ofMinutes(5))
                .timeForRest(Duration.ofMinutes(0))
                .build();

        taskRepository.save(secondTask);

        assertThrows(ConstraintViolationException.class, () -> taskService.getAllTasks(GetAllTasksRequest.builder()
                .userLogin(user.getLogin())
                .limit(5)
                .offset(-1)
                .build()));
    }

    @Test
    public void testGetAllTasks_whenOnlyUserLoginIsPassed() {
        User user = User.builder()
                .login("user_1")
                .password("password")
                .firstName("first_name")
                .build();

        userRepository.save(user);

        Project firstProject = Project.builder()
                .name("test_project_1")
                .goal("test_goal")
                .priority(1)
                .owner(user)
                .build();

        projectRepository.save(firstProject);

        Project secondProject = Project.builder()
                .name("test_project_2")
                .goal("test_goal")
                .priority(3)
                .owner(user)
                .build();

        projectRepository.save(secondProject);

        Task firstTask = Task.builder()
                .project(firstProject)
                .title("test_task_1")
                .description("test_description_1")
                .deadline(LocalDateTime.now().plus(Duration.ofMinutes(30)))
                .priority(1)
                .timeForCompletion(Duration.ofMinutes(15))
                .timeForRest(Duration.ofMinutes(5))
                .build();

        firstTask.setCompleted(true);

        taskRepository.save(firstTask);

        Task secondTask = Task.builder()
                .project(secondProject)
                .title("test_task_2")
                .description("test_description_2")
                .deadline(LocalDateTime.now().plus(Duration.ofMinutes(25)))
                .priority(5)
                .timeForCompletion(Duration.ofMinutes(5))
                .timeForRest(Duration.ofMinutes(0))
                .build();

        Task savedSecondTask = taskRepository.save(secondTask);

        List<TaskInfo> tasks = taskService.getAllTasks(GetAllTasksRequest.builder()
                .userLogin(user.getLogin())
                .build());

        assertEquals(1, tasks.size());
        assertTrue(tasks.contains(taskService.getTaskById(GetTaskByIdRequest.builder()
                .userLogin(user.getLogin())
                .taskId(savedSecondTask.getTaskId())
                .build())
        ));
    }

    @Test
    public void testGetAllTasks_whenCompletedIsPassedAndSetTrue() {
        User user = User.builder()
                .login("user_1")
                .password("password")
                .firstName("first_name")
                .build();

        userRepository.save(user);

        Project firstProject = Project.builder()
                .name("test_project_1")
                .goal("test_goal")
                .priority(1)
                .owner(user)
                .build();

        projectRepository.save(firstProject);

        Project secondProject = Project.builder()
                .name("test_project_2")
                .goal("test_goal")
                .priority(3)
                .owner(user)
                .build();

        projectRepository.save(secondProject);

        Task firstTask = Task.builder()
                .project(firstProject)
                .title("test_task_1")
                .description("test_description_1")
                .deadline(LocalDateTime.now().plus(Duration.ofMinutes(30)))
                .priority(1)
                .timeForCompletion(Duration.ofMinutes(15))
                .timeForRest(Duration.ofMinutes(5))
                .build();

        firstTask.setCompleted(true);

        Task savedFirstTask = taskRepository.save(firstTask);

        Task secondTask = Task.builder()
                .project(secondProject)
                .title("test_task_2")
                .description("test_description_2")
                .deadline(LocalDateTime.now().plus(Duration.ofMinutes(25)))
                .priority(5)
                .timeForCompletion(Duration.ofMinutes(5))
                .timeForRest(Duration.ofMinutes(0))
                .build();

        taskRepository.save(secondTask);

        List<TaskInfo> completedTasks = taskService.getAllTasks(GetAllTasksRequest.builder()
                .userLogin(user.getLogin())
                .completed(true)
                .build());

        assertEquals(1, completedTasks.size());
        assertTrue(completedTasks.contains(taskService.getTaskById(GetTaskByIdRequest.builder()
                .userLogin(user.getLogin())
                .taskId(savedFirstTask.getTaskId())
                .build())));
    }

    @Test
    public void testGetAllTasks_whenCompletedIsPassedAndSetFalse() {
        User user = User.builder()
                .login("user_1")
                .password("password")
                .firstName("first_name")
                .build();

        userRepository.save(user);

        Project firstProject = Project.builder()
                .name("test_project_1")
                .goal("test_goal")
                .priority(1)
                .owner(user)
                .build();

        projectRepository.save(firstProject);

        Project secondProject = Project.builder()
                .name("test_project_2")
                .goal("test_goal")
                .priority(3)
                .owner(user)
                .build();

        projectRepository.save(secondProject);

        Task firstTask = Task.builder()
                .project(firstProject)
                .title("test_task_1")
                .description("test_description_1")
                .deadline(LocalDateTime.now().plus(Duration.ofMinutes(30)))
                .priority(1)
                .timeForCompletion(Duration.ofMinutes(15))
                .timeForRest(Duration.ofMinutes(5))
                .build();

        firstTask.setCompleted(true);

        taskRepository.save(firstTask);

        Task secondTask = Task.builder()
                .project(secondProject)
                .title("test_task_2")
                .description("test_description_2")
                .deadline(LocalDateTime.now().plus(Duration.ofMinutes(25)))
                .priority(5)
                .timeForCompletion(Duration.ofMinutes(5))
                .timeForRest(Duration.ofMinutes(0))
                .build();

        Task savedSecondTask = taskRepository.save(secondTask);

        List<TaskInfo> completedTasks = taskService.getAllTasks(GetAllTasksRequest.builder()
                .userLogin(user.getLogin())
                .completed(false)
                .build());

        assertEquals(1, completedTasks.size());
        assertTrue(completedTasks.contains(taskService.getTaskById(GetTaskByIdRequest.builder()
                .userLogin(user.getLogin())
                .taskId(savedSecondTask.getTaskId())
                .build())));
    }

    @Test
    public void testGetAllTasks_whenCompletedIsPassedAndSetTrueAndDeadlineAfterIsPassed() {
        User user = User.builder()
                .login("user_1")
                .password("password")
                .firstName("first_name")
                .build();

        userRepository.save(user);

        Project firstProject = Project.builder()
                .name("test_project_1")
                .goal("test_goal")
                .priority(1)
                .owner(user)
                .build();

        projectRepository.save(firstProject);

        Project secondProject = Project.builder()
                .name("test_project_2")
                .goal("test_goal")
                .priority(3)
                .owner(user)
                .build();

        projectRepository.save(secondProject);

        Task firstTask = Task.builder()
                .project(firstProject)
                .title("test_task_1")
                .description("test_description_1")
                .deadline(LocalDateTime.now().plus(Duration.ofMinutes(30)))
                .priority(1)
                .timeForCompletion(Duration.ofMinutes(15))
                .timeForRest(Duration.ofMinutes(5))
                .build();

        firstTask.setCompleted(true);

        Task savedFirstTask = taskRepository.save(firstTask);

        Task secondTask = Task.builder()
                .project(secondProject)
                .title("test_task_2")
                .description("test_description_2")
                .deadline(LocalDateTime.now().plus(Duration.ofMinutes(25)))
                .priority(5)
                .timeForCompletion(Duration.ofMinutes(5))
                .timeForRest(Duration.ofMinutes(0))
                .build();

        taskRepository.save(secondTask);

        LocalDateTime currentDateTime = LocalDateTime.now();

        List<TaskInfo> tasks = taskService.getAllTasks(GetAllTasksRequest.builder()
                .userLogin(user.getLogin())
                .completed(true)
                .deadlineAfter(currentDateTime)
                .limit(2)
                .offset(0)
                .build());

        assertEquals(1, tasks.size());
        assertTrue(tasks.contains(taskService.getTaskById(GetTaskByIdRequest.builder()
                .userLogin(user.getLogin())
                .taskId(savedFirstTask.getTaskId())
                .build()))
        );
    }

    @Test
    public void testGetAllTasks_whenCompletedIsPassedAndSetFalseAndDeadlineAfterIsPassed() {
        User user = User.builder()
                .login("user_1")
                .password("password")
                .firstName("first_name")
                .build();

        userRepository.save(user);

        Project firstProject = Project.builder()
                .name("test_project_1")
                .goal("test_goal")
                .priority(1)
                .owner(user)
                .build();

        projectRepository.save(firstProject);

        Project secondProject = Project.builder()
                .name("test_project_2")
                .goal("test_goal")
                .priority(3)
                .owner(user)
                .build();

        projectRepository.save(secondProject);

        Task firstTask = Task.builder()
                .project(firstProject)
                .title("test_task_1")
                .description("test_description_1")
                .deadline(LocalDateTime.now().plus(Duration.ofMinutes(30)))
                .priority(1)
                .timeForCompletion(Duration.ofMinutes(15))
                .timeForRest(Duration.ofMinutes(5))
                .build();

        firstTask.setCompleted(true);

        taskRepository.save(firstTask);

        Task secondTask = Task.builder()
                .project(secondProject)
                .title("test_task_2")
                .description("test_description_2")
                .deadline(LocalDateTime.now().plus(Duration.ofMinutes(25)))
                .priority(5)
                .timeForCompletion(Duration.ofMinutes(5))
                .timeForRest(Duration.ofMinutes(0))
                .build();

        Task savedSecondTask = taskRepository.save(secondTask);

        LocalDateTime currentDateTime = LocalDateTime.now();

        List<TaskInfo> tasks = taskService.getAllTasks(GetAllTasksRequest.builder()
                .userLogin(user.getLogin())
                .completed(false)
                .deadlineAfter(currentDateTime)
                .limit(2)
                .offset(0)
                .build());

        assertEquals(1, tasks.size());
        assertTrue(tasks.contains(taskService.getTaskById(GetTaskByIdRequest.builder()
                .userLogin(user.getLogin())
                .taskId(savedSecondTask.getTaskId())
                .build())
        ));
    }

    @Test
    public void testCreateTask_whenProjectWithPassedIdDoesntExist() {
        assertThrows(EntityNotFoundException.class, () -> taskService.createTask(CreateTaskRequest.builder()
                .userLogin("user_1")
                .projectId(1L)
                .title("test_task")
                .description("test_description")
                .deadline(LocalDateTime.now().plusDays(3))
                .priority(1)
                .timeForCompletion(Duration.ofMinutes(30))
                .timeForRest(Duration.ofMinutes(5))
                .build()));
    }

    @Test
    public void testCreateTask_whenOwnerOfProjectWithPassedIdIsOtherUser() {
        User user = User.builder()
                .login("user_1")
                .password("password")
                .firstName("first_name")
                .build();

        userRepository.save(user);

        Project project = Project.builder()
                .name("test_project")
                .goal("test_goal")
                .priority(1)
                .owner(user)
                .build();

        Project savedProject = projectRepository.save(project);

        assertThrows(EntityNotFoundException.class, () -> taskService.createTask(CreateTaskRequest.builder()
                .userLogin("user_2")
                .projectId(savedProject.getProjectId())
                .title("test_task")
                .description("test_description")
                .deadline(LocalDateTime.now().plusDays(3))
                .priority(1)
                .timeForCompletion(Duration.ofMinutes(30))
                .timeForRest(Duration.ofMinutes(5))
                .build()));
    }

    @Test
    public void testCreateTask_whenEverythingIsOk() {
        User user = User.builder()
                .login("user_1")
                .password("password")
                .firstName("first_name")
                .build();

        userRepository.save(user);

        Project project = Project.builder()
                .name("test_project")
                .goal("test_goal")
                .priority(1)
                .owner(user)
                .build();

        Project savedProject = projectRepository.save(project);

        CreateTaskRequest request = CreateTaskRequest.builder()
                .userLogin(user.getLogin())
                .projectId(savedProject.getProjectId())
                .title("test_task")
                .description("test_description")
                .deadline(LocalDateTime.now().plusDays(3))
                .priority(1)
                .timeForCompletion(Duration.ofMinutes(30))
                .timeForRest(Duration.ofMinutes(5))
                .build();

        TaskInfo taskInfo = taskService.createTask(request);

        assertEquals(request.getProjectId(), taskInfo.getProjectId());
        assertEquals(request.getTitle(), taskInfo.getTitle());
        assertEquals(request.getDescription(), taskInfo.getDescription());
        assertEquals(request.getDeadline(), taskInfo.getDeadline());
        assertEquals(request.getPriority(), taskInfo.getPriority());
        assertEquals(request.getTimeForCompletion(), taskInfo.getTimeForCompletion());
        assertEquals(request.getTimeForRest(), taskInfo.getTimeForRest());
        assertFalse(taskInfo.isCompleted());
        assertNotNull(taskInfo.getCreationTime());
        assertNull(taskInfo.getUpdateTime());
    }

    @Test
    public void testUpdateTask_whenTaskWithPassedIdDoesntExist() {
        assertThrows(EntityNotFoundException.class, () -> taskService.updateTask(UpdateTaskRequest.builder()
                .userLogin("user_1")
                .taskId(1L)
                .title("test_task")
                .description("test_description")
                .deadline(LocalDateTime.now().plusDays(3))
                .priority(1)
                .timeForCompletion(Duration.ofMinutes(30))
                .completed(true)
                .timeForRest(Duration.ofMinutes(5))
                .build()));
    }

    @Test
    public void testUpdateTask_whenOwnerOfTaskIsOtherUser() {
        User user = User.builder()
                .login("user_1")
                .password("password")
                .firstName("first_name")
                .build();

        userRepository.save(user);

        Project project = Project.builder()
                .name("test_project")
                .goal("test_goal")
                .priority(1)
                .owner(user)
                .build();

        projectRepository.save(project);

        Task task = Task.builder()
                .project(project)
                .title("test_task")
                .description("test_description")
                .deadline(LocalDateTime.now().plus(Duration.ofMinutes(30)))
                .priority(1)
                .timeForCompletion(Duration.ofMinutes(15))
                .timeForRest(Duration.ofMinutes(5))
                .build();

        Task savedTask = taskRepository.save(task);

        assertThrows(EntityNotFoundException.class, () -> taskService.updateTask(UpdateTaskRequest.builder()
                .userLogin("user_2")
                .taskId(savedTask.getTaskId())
                .title("test_task")
                .description("test_description")
                .deadline(LocalDateTime.now().plusDays(3))
                .priority(1)
                .timeForCompletion(Duration.ofMinutes(30))
                .completed(true)
                .timeForRest(Duration.ofMinutes(5))
                .build()));
    }

    @Test
    public void testUpdateTask_whenEverythingIsOk() {
        User user = User.builder()
                .login("user_1")
                .password("password")
                .firstName("first_name")
                .build();

        userRepository.save(user);

        Project project = Project.builder()
                .name("test_project")
                .goal("test_goal")
                .priority(1)
                .owner(user)
                .build();

        projectRepository.save(project);

        Task task = Task.builder()
                .project(project)
                .title("test_task")
                .description("test_description")
                .deadline(LocalDateTime.now().plus(Duration.ofMinutes(30)))
                .priority(1)
                .timeForCompletion(Duration.ofMinutes(15))
                .timeForRest(Duration.ofMinutes(5))
                .build();

        Task savedTask = taskRepository.save(task);

        UpdateTaskRequest request = UpdateTaskRequest.builder()
                .userLogin(user.getLogin())
                .taskId(savedTask.getTaskId())
                .title("updated_test_task")
                .description("updated_test_description")
                .deadline(LocalDateTime.now().plusDays(5))
                .priority(3)
                .timeForCompletion(Duration.ofMinutes(50))
                .completed(true)
                .timeForRest(Duration.ofMinutes(15))
                .build();

        TaskInfo taskInfo = taskService.updateTask(request);

        assertEquals(task.getProject().getProjectId(), taskInfo.getProjectId());
        assertEquals(request.getTitle(), taskInfo.getTitle());
        assertEquals(request.getDescription(), taskInfo.getDescription());
        assertEquals(request.getDeadline(), taskInfo.getDeadline());
        assertEquals(request.getPriority(), taskInfo.getPriority());
        assertEquals(request.getTimeForCompletion(), taskInfo.getTimeForCompletion());
        assertEquals(request.getTimeForRest(), taskInfo.getTimeForRest());
        assertEquals(request.isCompleted(), taskInfo.isCompleted());
        assertEquals(savedTask.getCreationTime(), taskInfo.getCreationTime());
        assertNotNull(taskInfo.getUpdateTime());
    }

    @Test
    public void testSplitTask_whenTaskWithPassedIdDoesntExist() {
        assertThrows(EntityNotFoundException.class, () -> taskService.splitTask(SplitTaskRequest.builder()
                .userLogin("user_1")
                .taskId(1L)
                .subtasksCount(3)
                .build()));
    }

    @Test
    public void testSplitTask_whenOwnerOfTaskIsOtherUser() {
        User user = User.builder()
                .login("user_1")
                .password("password")
                .firstName("first_name")
                .build();

        userRepository.save(user);

        Project project = Project.builder()
                .name("test_project")
                .goal("test_goal")
                .priority(1)
                .owner(user)
                .build();

        projectRepository.save(project);

        Task task = Task.builder()
                .project(project)
                .title("test_task")
                .description("test_description")
                .deadline(LocalDateTime.now().plus(Duration.ofMinutes(30)))
                .priority(1)
                .timeForCompletion(Duration.ofMinutes(15))
                .timeForRest(Duration.ofMinutes(5))
                .build();

        Task savedTask = taskRepository.save(task);

        assertThrows(EntityNotFoundException.class, () -> taskService.splitTask(SplitTaskRequest.builder()
                .userLogin("user_2")
                .taskId(savedTask.getTaskId())
                .subtasksCount(3)
                .build()));
    }

    @Test
    public void testSplitTask_whenEverythingIsOk() {
        User user = User.builder()
                .login("user_1")
                .password("password")
                .firstName("first_name")
                .build();

        userRepository.save(user);

        Project project = Project.builder()
                .name("test_project")
                .goal("test_goal")
                .priority(1)
                .owner(user)
                .build();

        projectRepository.save(project);

        Task task = Task.builder()
                .project(project)
                .title("test_task")
                .description("test_description")
                .deadline(LocalDateTime.now().plus(Duration.ofMinutes(30)))
                .priority(1)
                .timeForCompletion(Duration.ofMinutes(15))
                .timeForRest(Duration.ofMinutes(5))
                .build();

        Task savedTask = taskRepository.save(task);

        SplitTaskRequest request = SplitTaskRequest.builder()
                .userLogin(user.getLogin())
                .taskId(savedTask.getTaskId())
                .subtasksCount(3)
                .build();

        List<TaskInfo> subtasks = taskService.splitTask(request);

        assertEquals(request.getSubtasksCount() - 1,
                subtasks.size());

        assertEquals(task.getProject().getProjectId(), subtasks.get(0).getProjectId());
        assertEquals(task.getTitle() + " (copy)", subtasks.get(0).getTitle());
        assertEquals(task.getDescription(), subtasks.get(0).getDescription());
        assertEquals(task.getDeadline(), subtasks.get(0).getDeadline());
        assertEquals(task.getPriority(), subtasks.get(0).getPriority());
        assertEquals(task.getTimeForCompletion(), subtasks.get(0).getTimeForCompletion());
        assertEquals(task.getTimeForRest(), subtasks.get(0).getTimeForRest());
        assertNotNull(subtasks.get(0).getCreationTime());
        assertNotEquals(task.getCreationTime(), subtasks.get(0).getCreationTime());
        assertNull(subtasks.get(0).getUpdateTime());
        assertEquals(task.isCompleted(), subtasks.get(0).isCompleted());

        assertEquals(task.getProject().getProjectId(), subtasks.get(1).getProjectId());
        assertEquals(task.getTitle() + " (copy)", subtasks.get(1).getTitle());
        assertEquals(task.getDescription(), subtasks.get(1).getDescription());
        assertEquals(task.getDeadline(), subtasks.get(1).getDeadline());
        assertEquals(task.getPriority(), subtasks.get(1).getPriority());
        assertEquals(task.getTimeForCompletion(), subtasks.get(1).getTimeForCompletion());
        assertEquals(task.getTimeForRest(), subtasks.get(1).getTimeForRest());
        assertNotNull(subtasks.get(1).getCreationTime());
        assertNotEquals(task.getCreationTime(), subtasks.get(1).getCreationTime());
        assertNull(subtasks.get(1).getUpdateTime());
        assertEquals(task.isCompleted(), subtasks.get(1).isCompleted());
    }

    @Test
    public void testDeleteTask_whenTaskWithPassedIdDoesntExist() {
        assertThrows(EntityNotFoundException.class, () -> taskService.deleteTask(DeleteTaskRequest.builder()
                .userLogin("user_1")
                .taskId(1L)
                .build()));
    }

    @Test
    public void testDeleteTask_whenOwnerOfTaskIsOtherUser() {
        User user = User.builder()
                .login("user_1")
                .password("password")
                .firstName("first_name")
                .build();

        userRepository.save(user);

        Project project = Project.builder()
                .name("test_project")
                .goal("test_goal")
                .priority(1)
                .owner(user)
                .build();

        projectRepository.save(project);

        Task task = Task.builder()
                .project(project)
                .title("test_task")
                .description("test_description")
                .deadline(LocalDateTime.now().plus(Duration.ofMinutes(30)))
                .priority(1)
                .timeForCompletion(Duration.ofMinutes(15))
                .timeForRest(Duration.ofMinutes(5))
                .build();

        Task savedTask = taskRepository.save(task);

        assertThrows(EntityNotFoundException.class, () -> taskService.deleteTask(DeleteTaskRequest.builder()
                .userLogin("user_2")
                .taskId(savedTask.getTaskId())
                .build()));
    }

    @Test
    public void testDeleteTask_whenEverythingIsOk() {
        User user = User.builder()
                .login("user_1")
                .password("password")
                .firstName("first_name")
                .build();

        userRepository.save(user);

        Project project = Project.builder()
                .name("test_project")
                .goal("test_goal")
                .priority(1)
                .owner(user)
                .build();

        projectRepository.save(project);

        Task task = Task.builder()
                .project(project)
                .title("test_task")
                .description("test_description")
                .deadline(LocalDateTime.now().plus(Duration.ofMinutes(30)))
                .priority(1)
                .timeForCompletion(Duration.ofMinutes(15))
                .timeForRest(Duration.ofMinutes(5))
                .build();

        Task savedTask = taskRepository.save(task);

        taskService.deleteTask(DeleteTaskRequest.builder()
                .userLogin(user.getLogin())
                .taskId(savedTask.getTaskId())
                .build());

        assertTrue(taskRepository.findById(savedTask.getTaskId()).isEmpty());
    }
}
