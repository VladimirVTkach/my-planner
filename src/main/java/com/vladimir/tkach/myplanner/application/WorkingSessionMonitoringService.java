package com.vladimir.tkach.myplanner.application;

import com.vladimir.tkach.myplanner.domain.planning.WorkingSession;
import com.vladimir.tkach.myplanner.domain.planning.WorkingSessionState;
import com.vladimir.tkach.myplanner.infrastructure.repository.WorkingSessionRepository;
import lombok.AllArgsConstructor;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class WorkingSessionMonitoringService {

    private final WorkingSessionRepository workingSessionRepository;

    @Scheduled(fixedDelay = 1000)
    public void finishWorkingSessions() {
        List<WorkingSession> finishedWorkingSessions = workingSessionRepository.findAllByState(WorkingSessionState.STARTED)
                .stream()
                .filter(WorkingSession::hasTimeElapsed)
                .peek(WorkingSession::finish)
                .toList();

        workingSessionRepository.saveAll(finishedWorkingSessions);
    }
}
