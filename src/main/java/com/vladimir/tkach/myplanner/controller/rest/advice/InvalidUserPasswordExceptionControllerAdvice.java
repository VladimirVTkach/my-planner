package com.vladimir.tkach.myplanner.controller.rest.advice;

import com.vladimir.tkach.myplanner.application.exception.InvalidUserPasswordException;
import com.vladimir.tkach.myplanner.controller.rest.dto.ErrorResponse;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.servlet.http.HttpServletRequest;

@AllArgsConstructor
@ControllerAdvice
public class InvalidUserPasswordExceptionControllerAdvice {

    private final GenericControllerAdvice genericControllerAdvice;

    @ExceptionHandler(value = InvalidUserPasswordException.class)
    public ResponseEntity<ErrorResponse> handle(InvalidUserPasswordException exception,
                                                HttpServletRequest request) {
        return genericControllerAdvice.handle(exception, request, HttpStatus.UNPROCESSABLE_ENTITY);
    }
}
