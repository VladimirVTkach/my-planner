package com.vladimir.tkach.myplanner.domain.planning;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Data
@NoArgsConstructor
public class UnitOfWork {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long unitOfWorkId;

    @ManyToOne
    @JoinColumn(name = "working_session_id")
    private WorkingSession workingSession;

    @ManyToOne
    @JoinColumn(name = "task_id")
    private Task task;

    private LocalDateTime startTime;

    private LocalDateTime endTime;

    public UnitOfWork(Task task) {
        this.task = task;
    }

    public void setTimingIntervals(LocalDateTime startTime) {
        this.startTime = startTime;
        this.endTime = startTime.plus(task.getTotalRequiredTime());
    }
}
