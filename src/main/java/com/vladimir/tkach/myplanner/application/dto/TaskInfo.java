package com.vladimir.tkach.myplanner.application.dto;

import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.Duration;
import java.time.LocalDateTime;

@Data
@NoArgsConstructor
public class TaskInfo {
    private Long taskId;
    private Long projectId;
    private String title;
    private String description;
    private LocalDateTime deadline;
    private Integer priority;
    private Duration timeForCompletion;
    private Duration timeForRest;
    private LocalDateTime creationTime;
    private LocalDateTime updateTime;
    private boolean completed;

    @Builder
    public TaskInfo(Long taskId,
                    Long projectId,
                    String title,
                    String description,
                    LocalDateTime deadline,
                    Integer priority,
                    Duration timeForCompletion,
                    Duration timeForRest,
                    LocalDateTime creationTime,
                    LocalDateTime updateTime,
                    boolean completed) {
        this.taskId = taskId;
        this.projectId = projectId;
        this.title = title;
        this.description = description;
        this.deadline = deadline;
        this.priority = priority;
        this.timeForCompletion = timeForCompletion;
        this.timeForRest = timeForRest;
        this.creationTime = creationTime;
        this.updateTime = updateTime;
        this.completed = completed;
    }
}
