package com.vladimir.tkach.myplanner.application;

import com.vladimir.tkach.myplanner.application.dto.*;
import com.vladimir.tkach.myplanner.application.exception.EntityNotFoundException;
import com.vladimir.tkach.myplanner.application.exception.ProjectWithSuchNameAlreadyExistsException;
import com.vladimir.tkach.myplanner.domain.user.Project;
import com.vladimir.tkach.myplanner.domain.user.User;
import com.vladimir.tkach.myplanner.infrastructure.repository.ProjectRepository;
import com.vladimir.tkach.myplanner.infrastructure.repository.UserRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
public class ProjectServiceTest {
    @Autowired
    private ProjectService projectService;

    @Autowired
    private ProjectRepository projectRepository;

    @Autowired
    private UserRepository userRepository;

    @BeforeEach
    public void cleanUp() {
        projectRepository.deleteAll();
        userRepository.deleteAll();
    }

    @Test
    public void testGetProjectById_whenProjectWithPassedIdDoesNotExist() {
        assertThrows(EntityNotFoundException.class, () -> projectService.getProjectById(GetProjectByIdRequest.builder()
                .ownerLogin("test")
                .projectId(1L)
                .build()));
    }

    @Test
    public void testGetProjectById_whenOwnerOfProjectWithPassedIdIsOtherUser() {
        User user = User.builder()
                .login("user_1")
                .password("password")
                .firstName("first_name")
                .build();

        userRepository.save(user);

        Project project = Project.builder()
                .name("test_project")
                .goal("test_goal")
                .priority(1)
                .owner(user)
                .build();

        Project savedProject = projectRepository.save(project);

        assertThrows(EntityNotFoundException.class, () -> projectService.getProjectById(GetProjectByIdRequest.builder()
                .ownerLogin("user_2")
                .projectId(savedProject.getProjectId())
                .build()));
    }

    @Test
    public void testGetProjectById_whenEverythingIsOk() {
        User user = User.builder()
                .login("user_1")
                .password("password")
                .firstName("first_name")
                .build();

        userRepository.save(user);

        Project project = Project.builder()
                .name("test_project")
                .goal("test_goal")
                .priority(1)
                .owner(user)
                .build();

        Project savedProject = projectRepository.save(project);

        ProjectInfo projectInfo = projectService.getProjectById(GetProjectByIdRequest.builder()
                .ownerLogin(user.getLogin())
                .projectId(savedProject.getProjectId())
                .build());

        assertEquals(project.getProjectId(), projectInfo.getProjectId());
        assertEquals(project.getName(), projectInfo.getName());
        assertEquals(project.getGoal(), projectInfo.getGoal());
        assertEquals(project.getPriority(), projectInfo.getPriority());
        assertEquals(project.getCreationTime(), projectInfo.getCreationTime());
        assertEquals(project.getUpdateTime(), projectInfo.getUpdateTime());
        assertEquals(user.getLogin(), projectInfo.getOwnerLogin());
    }

    @Test
    public void testGetUserProjects_whenNameAndLimitAndOffsetParametersAreNotPassed() {
        User user = User.builder()
                .login("user_1")
                .password("password")
                .firstName("first_name")
                .build();

        userRepository.save(user);

        Project firstProject = Project.builder()
                .name("test_project_1")
                .goal("test_goal")
                .priority(1)
                .owner(user)
                .build();

        Project savedFirstProject = projectRepository.save(firstProject);

        Project secondProject = Project.builder()
                .name("test_project_2")
                .goal("test_goal")
                .priority(2)
                .owner(user)
                .build();

        Project savedSecondProject = projectRepository.save(secondProject);

        List<ProjectInfo> userProjects = projectService.getUserProjects(GetUserProjectsRequest.builder()
                .ownerLogin(user.getLogin())
                .build());

        assertTrue(userProjects.contains(projectService.getProjectById(GetProjectByIdRequest.builder()
                .ownerLogin(user.getLogin())
                .projectId(savedFirstProject.getProjectId())
                .build())));

        assertTrue(userProjects.contains(projectService.getProjectById(GetProjectByIdRequest.builder()
                .ownerLogin(user.getLogin())
                .projectId(savedSecondProject.getProjectId())
                .build())));
    }

    @Test
    public void testGetUserProjects_whenNameParameterIsPassedAndLimitAndOffsetParametersAreNotPassed() {
        User user = User.builder()
                .login("user_1")
                .password("password")
                .firstName("first_name")
                .build();

        userRepository.save(user);

        Project firstProject = Project.builder()
                .name("test_project_1")
                .goal("test_goal")
                .priority(1)
                .owner(user)
                .build();

        Project savedFirstProject = projectRepository.save(firstProject);

        Project secondProject = Project.builder()
                .name("test_project_2")
                .goal("test_goal")
                .priority(2)
                .owner(user)
                .build();

        Project savedSecondProject = projectRepository.save(secondProject);

        List<ProjectInfo> userProjects = projectService.getUserProjects(GetUserProjectsRequest.builder()
                .ownerLogin(user.getLogin())
                .name(firstProject.getName())
                .build());

        assertTrue(userProjects.contains(projectService.getProjectById(GetProjectByIdRequest.builder()
                .ownerLogin(user.getLogin())
                .projectId(savedFirstProject.getProjectId())
                .build())));

        assertFalse(userProjects.contains(projectService.getProjectById(GetProjectByIdRequest.builder()
                .ownerLogin(user.getLogin())
                .projectId(savedSecondProject.getProjectId())
                .build())));
    }

    @Test
    public void testGetUserProjects_whenNameParameterIsNotPassedAndLimitAndOffsetParametersArePassed() {
        User user = User.builder()
                .login("user_1")
                .password("password")
                .firstName("first_name")
                .build();

        userRepository.save(user);

        Project firstProject = Project.builder()
                .name("test_project_1")
                .goal("test_goal")
                .priority(1)
                .owner(user)
                .build();

        projectRepository.save(firstProject);

        Project secondProject = Project.builder()
                .name("test_project_2")
                .goal("test_goal")
                .priority(2)
                .owner(user)
                .build();

        projectRepository.save(secondProject);

        List<ProjectInfo> userProjects = projectService.getUserProjects(GetUserProjectsRequest.builder()
                .ownerLogin(user.getLogin())
                .limit(1)
                .offset(0)
                .build());

        assertEquals(1, userProjects.size());
    }

    @Test
    public void testCreateProject_withAlreadyOccupiedName() {
        User user = User.builder()
                .login("user_1")
                .password("password")
                .firstName("first_name")
                .build();

        userRepository.save(user);

        projectService.createProject(CreateProjectRequest.builder()
                .ownerLogin(user.getLogin())
                .name("test_project")
                .goal("test_goal")
                .priority(1)
                .build());

        assertThrows(ProjectWithSuchNameAlreadyExistsException.class,
                () -> projectService.createProject(CreateProjectRequest.builder()
                        .ownerLogin(user.getLogin())
                        .name("test_project")
                        .goal("test_goal_2")
                        .priority(5)
                        .build()));

    }

    @Test
    public void testCreateProject_whenEverythingIsOk() {
        User user = User.builder()
                .login("user_1")
                .password("password")
                .firstName("first_name")
                .build();

        userRepository.save(user);

        CreateProjectRequest request = CreateProjectRequest.builder()
                .ownerLogin(user.getLogin())
                .name("test_project")
                .goal("test_goal")
                .priority(1)
                .build();

        ProjectInfo createdProject = projectService.createProject(request);

        assertEquals(request.getName(), createdProject.getName());
        assertEquals(request.getGoal(), createdProject.getGoal());
        assertEquals(request.getPriority(), createdProject.getPriority());
        assertNotNull(createdProject.getCreationTime());
        assertNull(createdProject.getUpdateTime());
        assertEquals(request.getOwnerLogin(), createdProject.getOwnerLogin());
    }

    @Test
    public void testUpdateProject_whenProjectWithPassedIdDoesNotExist() {
        assertThrows(EntityNotFoundException.class, () -> projectService.updateProject(UpdateProjectRequest.builder()
                .ownerLogin("user_1")
                .projectId(1L)
                .name("new_name")
                .goal("new_goal")
                .priority(1)
                .build()));
    }

    @Test
    public void testUpdateProject_whenOwnerOfProjectWithPassedIdIsOtherUser() {
        User user = User.builder()
                .login("user_1")
                .password("password")
                .firstName("first_name")
                .build();

        userRepository.save(user);

        Project project = Project.builder()
                .name("test_project")
                .goal("test_goal")
                .priority(1)
                .owner(user)
                .build();

        Project savedProject = projectRepository.save(project);

        assertThrows(EntityNotFoundException.class, () -> projectService.updateProject(UpdateProjectRequest.builder()
                .ownerLogin("user_2")
                .projectId(savedProject.getProjectId())
                .name("new_name")
                .goal("new_goal")
                .priority(1)
                .build()));
    }

    @Test
    public void testUpdateProject_whenNameIsAlreadyOccupied() {
        User user = User.builder()
                .login("user_1")
                .password("password")
                .firstName("first_name")
                .build();

        userRepository.save(user);

        Project firstProject = Project.builder()
                .name("test_project_1")
                .goal("test_goal")
                .priority(1)
                .owner(user)
                .build();

        projectRepository.save(firstProject);

        Project secondProject = Project.builder()
                .name("test_project_2")
                .goal("test_goal")
                .priority(1)
                .owner(user)
                .build();

        Project savedSecondProject = projectRepository.save(secondProject);

        assertThrows(ProjectWithSuchNameAlreadyExistsException.class,
                () -> projectService.updateProject(UpdateProjectRequest.builder()
                        .ownerLogin(user.getLogin())
                        .projectId(savedSecondProject.getProjectId())
                        .name("test_project_1")
                        .goal("new_goal")
                        .priority(2)
                        .build()));
    }

    @Test
    public void testUpdateProject_whenEverythingIsOk() {
        User user = User.builder()
                .login("user_1")
                .password("password")
                .firstName("first_name")
                .build();

        userRepository.save(user);

        Project project = Project.builder()
                .name("test_project")
                .goal("test_goal")
                .priority(1)
                .owner(user)
                .build();

        Project savedProject = projectRepository.save(project);

        UpdateProjectRequest request = UpdateProjectRequest.builder()
                .ownerLogin(user.getLogin())
                .projectId(savedProject.getProjectId())
                .name("new_name")
                .goal("new_goal")
                .priority(2)
                .build();

        ProjectInfo updatedProject = projectService.updateProject(request);

        assertEquals(request.getName(), updatedProject.getName());
        assertEquals(request.getGoal(), updatedProject.getGoal());
        assertEquals(request.getPriority(), updatedProject.getPriority());
        assertEquals(project.getCreationTime(), updatedProject.getCreationTime());
        assertNotNull(updatedProject.getUpdateTime());
        assertEquals(request.getOwnerLogin(), updatedProject.getOwnerLogin());
    }

    @Test
    public void testDeleteProject_whenProjectWithPassedIdDoesNotExist() {
        assertThrows(EntityNotFoundException.class, () -> projectService.deleteProject(DeleteProjectRequest.builder()
                .ownerLogin("user_1")
                .projectId(1L)
                .build()));
    }

    @Test
    public void testDeleteProject_whenOwnerOfProjectWithPassedIdIsOtherUser() {
        User user = User.builder()
                .login("user_1")
                .password("password")
                .firstName("first_name")
                .build();

        userRepository.save(user);

        Project project = Project.builder()
                .name("test_project")
                .goal("test_goal")
                .priority(1)
                .owner(user)
                .build();

        Project savedProject = projectRepository.save(project);

        assertThrows(EntityNotFoundException.class, () -> projectService.deleteProject(DeleteProjectRequest.builder()
                .ownerLogin("user_2")
                .projectId(savedProject.getProjectId())
                .build()));
    }

    @Test
    public void testDeleteProject_whenEverythingIsOk() {
        User user = User.builder()
                .login("user_1")
                .password("password")
                .firstName("first_name")
                .build();

        userRepository.save(user);

        Project project = Project.builder()
                .name("test_project")
                .goal("test_goal")
                .priority(1)
                .owner(user)
                .build();

        Project savedProject = projectRepository.save(project);

        projectService.deleteProject(DeleteProjectRequest.builder()
                .ownerLogin(user.getLogin())
                .projectId(savedProject.getProjectId())
                .build());

        assertTrue(projectRepository.findById(savedProject.getProjectId()).isEmpty());
    }
}
