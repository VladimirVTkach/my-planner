package com.vladimir.tkach.myplanner.application.dto;

import lombok.Builder;
import lombok.Data;
import lombok.NonNull;

import javax.validation.constraints.NotNull;
import java.time.Duration;

@Data
@Builder
public class CreateWorkingSessionRequest {
    @NonNull
    private String ownerLogin;

    @NotNull(message = "duration can't be null")
    private Duration duration;
}