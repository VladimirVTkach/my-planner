package com.vladimir.tkach.myplanner.application.dto;

import lombok.Builder;
import lombok.Data;
import lombok.NonNull;

import javax.validation.constraints.NotNull;

@Data
@Builder
public class CreateProjectRequest {
    @NonNull
    private String ownerLogin;

    @NotNull(message = "name can't be null")
    private String name;

    private String goal;

    @NotNull(message = "priority can't be null")
    private Integer priority;
}
