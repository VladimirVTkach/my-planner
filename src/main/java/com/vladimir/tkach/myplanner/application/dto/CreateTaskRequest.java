package com.vladimir.tkach.myplanner.application.dto;

import lombok.Builder;
import lombok.Data;
import lombok.NonNull;

import javax.validation.constraints.NotNull;
import java.time.Duration;
import java.time.LocalDateTime;

@Data
@Builder
public class CreateTaskRequest {
    @NonNull
    private String userLogin;

    @NotNull(message = "project id can't be null")
    private Long projectId;

    @NotNull(message = "title can't be null")
    private String title;

    private String description;

    @NotNull(message = "deadline can't be null")
    private LocalDateTime deadline;

    @NotNull(message = "priority can't be null")
    private Integer priority;

    @NotNull(message = "time for completion can't be null")
    private Duration timeForCompletion;

    private Duration timeForRest;
}
