package com.vladimir.tkach.myplanner.domain.planning.exception;

public class InvalidTaskSplitException extends PlanningException {
    public InvalidTaskSplitException(String message) {
        super(message);
    }
}
