package com.vladimir.tkach.myplanner.domain.planning.exception;

public class DisallowedUnitOfWorkException extends PlanningException {
    public DisallowedUnitOfWorkException(String message) {
        super(message);
    }
}
