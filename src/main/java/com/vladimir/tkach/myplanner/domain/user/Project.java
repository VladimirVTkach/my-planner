package com.vladimir.tkach.myplanner.domain.user;

import com.vladimir.tkach.myplanner.domain.user.exception.InvalidProjectGoalException;
import com.vladimir.tkach.myplanner.domain.user.exception.InvalidProjectPriorityException;
import com.vladimir.tkach.myplanner.domain.user.exception.InvalidProjectNameException;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Data
@NoArgsConstructor
public class Project {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long projectId;

    private String name;

    private String goal;

    private Integer priority;

    private LocalDateTime creationTime;

    private LocalDateTime updateTime;

    @ManyToOne
    @JoinColumn(name = "user_id")
    @NonNull
    private User owner;

    @Builder
    public Project(@NonNull String name,
                   String goal,
                   @NonNull Integer priority,
                   @NonNull User owner) {
        this.name = requireValidName(name);
        this.goal = requireValidGoal(goal);
        this.priority = requireValidPriority(priority);
        this.owner = owner;
    }

    public void setName(@NonNull String name) {
        this.name = requireValidName(name);
    }

    public void setGoal(String goal) {
        this.goal = requireValidGoal(goal);
    }

    public void setPriority(@NonNull Integer priority) {
        this.priority = requireValidPriority(priority);
    }

    @PrePersist
    private void setCreationTime() {
        this.creationTime = LocalDateTime.now();
    }

    @PreUpdate
    private void setUpdateTime() {
        this.updateTime = LocalDateTime.now();
    }

    private String requireValidName(String name) {
        if (name.isBlank()) {
            throw new InvalidProjectNameException("name must be non-empty");
        }

        if(name.length() > 30) {
            throw new InvalidProjectNameException("max allowed name length is 30 characters");
        }

        return name;
    }

    private String requireValidGoal(String goal) {
        if (goal != null && goal.isBlank()) {
            throw new InvalidProjectGoalException("goal must be non-empty");
        }

        if(goal != null && goal.length() > 255) {
            throw new InvalidProjectNameException("max allowed name length is 255 characters");
        }

        return goal;
    }

    private int requireValidPriority(int priority) {
        if (priority <= 0) {
            throw new InvalidProjectPriorityException("priority must be positive");
        }

        return priority;
    }
}
