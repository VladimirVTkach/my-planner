package com.vladimir.tkach.myplanner.controller.rest.dto;

import lombok.Data;
import org.springframework.boot.convert.DurationFormat;
import org.springframework.boot.convert.DurationStyle;

import java.time.Duration;

@Data
public class CreateWorkingSessionControllerRequest {
    @DurationFormat(value = DurationStyle.ISO8601)
    private Duration duration;
}
