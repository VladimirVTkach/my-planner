package com.vladimir.tkach.myplanner.application.dto;

import lombok.Builder;
import lombok.Data;
import lombok.NonNull;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.PositiveOrZero;
import java.util.Objects;

@Data
@Builder
public class GetUserProjectsByName {
    @NonNull
    private String ownerLogin;

    @NotNull(message = "name can't be null")
    private String name;

    @Positive(message = "limit must be positive")
    private Integer limit;

    @PositiveOrZero(message = "offset must be non-negative")
    private Integer offset;

    public Integer getLimit() {
        return Objects.requireNonNullElse(limit, 10);
    }

    public Integer getOffset() {
        return Objects.requireNonNullElse(offset, 0);
    }
}
