package com.vladimir.tkach.myplanner.application.dto;

import lombok.Builder;
import lombok.Data;
import lombok.NonNull;

import javax.validation.constraints.Positive;
import javax.validation.constraints.PositiveOrZero;
import java.util.Objects;

@Data
@Builder
public class GetUserProjectsRequest {
    @NonNull
    private String ownerLogin;

    private String name;

    @Positive(message = "limit must be positive")
    private Integer limit;

    @PositiveOrZero(message = "offset must be non-negative")
    private Integer offset;
}
