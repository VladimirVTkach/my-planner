package com.vladimir.tkach.myplanner.controller.rest.advice;

import com.vladimir.tkach.myplanner.controller.rest.dto.ErrorResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;

@Component
public class GenericControllerAdvice {
    public ResponseEntity<ErrorResponse> handle(Exception exception,
                                                HttpServletRequest request,
                                                HttpStatus responseStatus) {
        return ResponseEntity.status(responseStatus)
                .body(ErrorResponse.builder()
                        .timestamp(LocalDateTime.now())
                        .status(responseStatus.value())
                        .error(exception.getMessage())
                        .path(request.getRequestURI())
                        .build());
    }
}
