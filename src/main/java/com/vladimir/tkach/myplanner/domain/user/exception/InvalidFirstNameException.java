package com.vladimir.tkach.myplanner.domain.user.exception;

public class InvalidFirstNameException extends UserException {
    public InvalidFirstNameException(String message) {
        super(message);
    }
}
