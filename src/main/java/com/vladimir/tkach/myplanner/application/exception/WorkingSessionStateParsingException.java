package com.vladimir.tkach.myplanner.application.exception;

public class WorkingSessionStateParsingException extends RuntimeException {
    public WorkingSessionStateParsingException(String message) {
        super(message);
    }
}
