package com.vladimir.tkach.myplanner.domain.planning;

import com.vladimir.tkach.myplanner.domain.planning.exception.DisallowedUnitOfWorkException;
import com.vladimir.tkach.myplanner.domain.planning.exception.InvalidStateTransitionException;
import com.vladimir.tkach.myplanner.domain.user.Project;
import com.vladimir.tkach.myplanner.domain.user.User;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.time.Duration;
import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.*;

public class WorkingSessionTest {
    @Test
    public void testIsAllowedUnitOfWork_whenUnitOfWorkIsAllowed() {
        User user = new User("login",
                "password",
                "John");

        WorkingSession workingSession = new WorkingSession(user,
                Duration.ofMinutes(25));

        Project project = Project.builder()
                .name("test_project")
                .goal("test_goal")
                .priority(1)
                .owner(user)
                .build();

        Task task = Task.builder()
                .project(project)
                .title("test_task")
                .description("test_description")
                .deadline(LocalDateTime.now().plus(Duration.ofDays(30)))
                .priority(10)
                .timeForCompletion(Duration.ofMinutes(15))
                .timeForRest(Duration.ZERO)
                .build();

        UnitOfWork unitOfWork = new UnitOfWork(task);

        assertTrue(workingSession.isAllowedUnitOfWork(unitOfWork));
    }

    @Test
    public void testIsAllowedUnitOfWork_whenUnitOfWorkIsNotAllowed() {
        User user = new User("login",
                "password",
                "John");

        WorkingSession workingSession = new WorkingSession(user,
                Duration.ofMinutes(40));

        Project project = Project.builder()
                .name("test_project")
                .goal("test_goal")
                .priority(1)
                .owner(user)
                .build();

        Task task = Task.builder()
                .project(project)
                .title("test_task")
                .description("test_description")
                .deadline(LocalDateTime.now().plus(Duration.ofHours(10)))
                .priority(1)
                .timeForCompletion(Duration.ofMinutes(50))
                .timeForRest(Duration.ofMinutes(30))
                .build();

        UnitOfWork unitOfWork = new UnitOfWork(task);

        assertFalse(workingSession.isAllowedUnitOfWork(unitOfWork));
    }

    @Test
    public void testAddUnitOfWork_whenUnitOfWorkIsAllowed() {
        User user = new User("login",
                "password",
                "John");

        WorkingSession workingSession = new WorkingSession(user,
                Duration.ofMinutes(20));

        Project project = Project.builder()
                .name("test_project")
                .goal("test_goal")
                .priority(1)
                .owner(user)
                .build();

        Task task = Task.builder()
                .project(project)
                .title("test_task")
                .description("test_description")
                .deadline(LocalDateTime.now().plus(Duration.ofDays(1)))
                .priority(2)
                .timeForCompletion(Duration.ofMinutes(10))
                .timeForRest(Duration.ofMinutes(5))
                .build();

        UnitOfWork unitOfWork = new UnitOfWork(task);

        workingSession.addUnitOfWork(unitOfWork);

        assertTrue(workingSession.getUnitsOfWork().contains(unitOfWork));
    }

    @Test
    public void testAddUnitOfWork_whenUnitOfWorkIsNotAllowed() {
        User user = new User("login",
                "password",
                "John");

        WorkingSession workingSession = new WorkingSession(user,
                Duration.ofMinutes(25));

        Project project = Project.builder()
                .name("test_project")
                .goal("test_goal")
                .priority(1)
                .owner(user)
                .build();

        Task task = Task.builder()
                .project(project)
                .title("test_task")
                .description("test_description")
                .deadline(LocalDateTime.now().plus(Duration.ofHours(12)))
                .priority(1)
                .timeForCompletion(Duration.ofMinutes(25))
                .timeForRest(Duration.ofMinutes(5))
                .build();

        UnitOfWork unitOfWork = new UnitOfWork(task);

        assertThrows(DisallowedUnitOfWorkException.class,
                () -> workingSession.addUnitOfWork(unitOfWork));
    }

    @Test
    public void testAdjustDuration_caseOne() {
        User user = User.builder()
                .login("user")
                .password("password")
                .firstName("John")
                .build();

        WorkingSession workingSession = new WorkingSession(user,
                Duration.ofHours(2));

        Project project = Project.builder()
                .name("test_project")
                .goal("test_goal")
                .priority(1)
                .owner(user)
                .build();

        Task firstTask = Task.builder()
                .project(project)
                .title("test_task_1")
                .description("test_description")
                .deadline(LocalDateTime.now().plus(Duration.ofHours(1)))
                .priority(1)
                .timeForCompletion(Duration.ofMinutes(10))
                .timeForRest(Duration.ofMinutes(5))
                .build();

        Task secondTask = Task.builder()
                .project(project)
                .title("test_task_2")
                .description("test_description")
                .deadline(LocalDateTime.now().plus(Duration.ofHours(3)))
                .priority(2)
                .timeForCompletion(Duration.ofHours(1))
                .timeForRest(Duration.ofMinutes(25))
                .build();

        workingSession.addUnitOfWork(new UnitOfWork(firstTask));
        workingSession.addUnitOfWork(new UnitOfWork(secondTask));

        workingSession.adjustDuration();

        assertEquals(workingSession.getActualDuration(),
                workingSession.getSpecifiedDuration());
    }

    @Test
    public void testAdjustDuration_caseTwo() {
        User user = User.builder()
                .login("user")
                .password("password")
                .firstName("John")
                .build();

        WorkingSession workingSession = new WorkingSession(user,
                Duration.ofMinutes(115));

        Project project = Project.builder()
                .name("test_project")
                .goal("test_goal")
                .priority(1)
                .owner(user)
                .build();

        Task firstTask = Task.builder()
                .project(project)
                .title("test_task_1")
                .description("test_description")
                .deadline(LocalDateTime.now().plus(Duration.ofHours(1)))
                .priority(1)
                .timeForCompletion(Duration.ofMinutes(10))
                .timeForRest(Duration.ofMinutes(5))
                .build();

        Task secondTask = Task.builder()
                .project(project)
                .title("test_task_2")
                .description("test_description")
                .deadline(LocalDateTime.now().plus(Duration.ofHours(3)))
                .priority(2)
                .timeForCompletion(Duration.ofHours(1))
                .timeForRest(Duration.ofMinutes(25))
                .build();

        workingSession.addUnitOfWork(new UnitOfWork(firstTask));
        workingSession.addUnitOfWork(new UnitOfWork(secondTask));

        workingSession.adjustDuration();

        assertEquals(workingSession.getActualDuration(),
                workingSession.getSpecifiedDuration());
    }

    @Test
    public void testHasTimeElapsed_whenItHasElapsed() {
        User user = new User("login",
                "password",
                "John");

        WorkingSession workingSession = new WorkingSession(user,
                Duration.ofMinutes(10));

        workingSession.start();

        assertTrue(workingSession.hasTimeElapsed());
    }

    @Test
    public void testHasTimeElapsed_whenItHasNotElapsed() {
        User user = new User("login",
                "password",
                "John");

        WorkingSession workingSession = new WorkingSession(user,
                Duration.ofMinutes(20));

        Project project = Project.builder()
                .name("test_project")
                .goal("test_goal")
                .priority(1)
                .owner(user)
                .build();

        Task task = Task.builder()
                .project(project)
                .title("test_task")
                .description("test_description")
                .deadline(LocalDateTime.now().plus(Duration.ofDays(5)))
                .priority(1)
                .timeForCompletion(Duration.ofMinutes(5))
                .timeForRest(Duration.ofMinutes(5))
                .build();

        UnitOfWork unitOfWork = new UnitOfWork(task);

        workingSession.addUnitOfWork(unitOfWork);

        workingSession.start();

        assertFalse(workingSession.hasTimeElapsed());
    }

    @Test
    public void testStartWorkingSession_whenItWasCreated() {
        User user = new User("login",
                "password",
                "John");

        WorkingSession workingSession = new WorkingSession(user,
                Duration.ofHours(2));

        Project project = Project.builder()
                .name("test_project")
                .goal("test_goal")
                .priority(1)
                .owner(user)
                .build();

        Task firstTask = Task.builder()
                .project(project)
                .title("test_task_1")
                .description("test_description")
                .deadline(LocalDateTime.now().plus(Duration.ofDays(5)))
                .priority(1)
                .timeForCompletion(Duration.ofMinutes(40))
                .timeForRest(Duration.ofMinutes(25))
                .build();

        Task secondTask = Task.builder()
                .project(project)
                .title("test_task_2")
                .description("test_description")
                .deadline(LocalDateTime.now().plus(Duration.ofDays(15)))
                .priority(2)
                .timeForCompletion(Duration.ofMinutes(25))
                .timeForRest(Duration.ofMinutes(5))
                .build();

        UnitOfWork firstUnitOfWork = new UnitOfWork(firstTask);
        UnitOfWork secondUnitOfWork = new UnitOfWork(secondTask);

        workingSession.addUnitOfWork(firstUnitOfWork);
        workingSession.addUnitOfWork(secondUnitOfWork);

        workingSession.start();

        WorkingSessionState expectedWorkingSessionState = WorkingSessionState.STARTED;

        assertEquals(expectedWorkingSessionState, workingSession.getState());

        Assertions.assertNotNull(workingSession.getUnitsOfWork().get(0).getStartTime());
        Assertions.assertNotNull(workingSession.getUnitsOfWork().get(0).getEndTime());

        Assertions.assertNotNull(workingSession.getUnitsOfWork().get(1).getStartTime());
        Assertions.assertNotNull(workingSession.getUnitsOfWork().get(1).getEndTime());
    }

    @Test
    public void testStartWorkingSession_whenItWasFinished() {
        User user = new User("login",
                "password",
                "John");

        WorkingSession workingSession = new WorkingSession(user,
                Duration.ofMinutes(30));

        Project project = Project.builder()
                .name("test_project")
                .goal("test_goal")
                .priority(1)
                .owner(user)
                .build();

        Task task = Task.builder()
                .project(project)
                .title("test_task")
                .description("test_description")
                .deadline(LocalDateTime.now().plus(Duration.ofDays(1)))
                .priority(2)
                .timeForCompletion(Duration.ofMinutes(15))
                .timeForRest(Duration.ofMinutes(5))
                .build();

        UnitOfWork unitOfWork = new UnitOfWork(task);

        workingSession.addUnitOfWork(unitOfWork);

        workingSession.start();
        workingSession.finish();

        Assertions.assertThrows(InvalidStateTransitionException.class,
                workingSession::start);
    }

    @Test
    public void testStartWorkingSession_whenItWasAlreadyStarted() {
        User user = new User("login",
                "password",
                "John");

        WorkingSession workingSession = new WorkingSession(user,
                Duration.ofMinutes(30));

        Project project = Project.builder()
                .name("test_project")
                .goal("test_goal")
                .priority(1)
                .owner(user)
                .build();

        Task task = Task.builder()
                .project(project)
                .title("test_task")
                .description("test_description")
                .deadline(LocalDateTime.now().plus(Duration.ofDays(1)))
                .priority(2)
                .timeForCompletion(Duration.ofMinutes(15))
                .timeForRest(Duration.ofMinutes(5))
                .build();

        UnitOfWork unitOfWork = new UnitOfWork(task);

        workingSession.addUnitOfWork(unitOfWork);

        workingSession.start();

        Assertions.assertThrows(InvalidStateTransitionException.class,
                workingSession::start);
    }

    @Test
    public void testFinishWorkingSession_whenItWasStarted() {
        User user = new User("login",
                "password",
                "John");

        WorkingSession workingSession = new WorkingSession(user,
                Duration.ofHours(1));

        Project project = Project.builder()
                .name("test_project")
                .goal("test_goal")
                .priority(1)
                .owner(user)
                .build();

        Task firstTask = Task.builder()
                .project(project)
                .title("test_task_1")
                .description("test_description")
                .deadline(LocalDateTime.now().plus(Duration.ofHours(15)))
                .priority(2)
                .timeForCompletion(Duration.ofMinutes(10))
                .timeForRest(Duration.ofMinutes(5))
                .build();

        Task secondTask = Task.builder()
                .project(project)
                .title("test_task_2")
                .description("test_description")
                .deadline(LocalDateTime.now().plus(Duration.ofDays(20)))
                .priority(3)
                .timeForCompletion(Duration.ofMinutes(30))
                .timeForRest(Duration.ofMinutes(10))
                .build();

        UnitOfWork firstUnitOfWork = new UnitOfWork(firstTask);
        UnitOfWork secondUnitOfWork = new UnitOfWork(secondTask);

        workingSession.addUnitOfWork(firstUnitOfWork);
        workingSession.addUnitOfWork(secondUnitOfWork);

        workingSession.start();

        workingSession.finish();

        WorkingSessionState expectedWorkingSessionState = WorkingSessionState.FINISHED;

        assertEquals(expectedWorkingSessionState, workingSession.getState());
    }

    @Test
    public void testFinishWorkingSession_whenItWasNotStarted() {
        User user = new User("login",
                "password",
                "John");

        WorkingSession workingSession = new WorkingSession(user,
                Duration.ofHours(1));

        Project project = Project.builder()
                .name("test_project")
                .goal("test_goal")
                .priority(1)
                .owner(user)
                .build();

        Task firstTask = Task.builder()
                .project(project)
                .title("test_task_1")
                .description("test_description")
                .deadline(LocalDateTime.now().plus(Duration.ofHours(15)))
                .priority(2)
                .timeForCompletion(Duration.ofMinutes(10))
                .timeForRest(Duration.ofMinutes(5))
                .build();

        Task secondTask = Task.builder()
                .project(project)
                .title("test_task_2")
                .description("test_description")
                .deadline(LocalDateTime.now().plus(Duration.ofDays(20)))
                .priority(3)
                .timeForCompletion(Duration.ofMinutes(30))
                .timeForRest(Duration.ofMinutes(10))
                .build();

        UnitOfWork firstUnitOfWork = new UnitOfWork(firstTask);
        UnitOfWork secondUnitOfWork = new UnitOfWork(secondTask);

        workingSession.addUnitOfWork(firstUnitOfWork);
        workingSession.addUnitOfWork(secondUnitOfWork);

        Assertions.assertThrows(InvalidStateTransitionException.class,
                workingSession::finish);
    }
}
