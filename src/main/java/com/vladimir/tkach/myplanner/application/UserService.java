package com.vladimir.tkach.myplanner.application;

import com.vladimir.tkach.myplanner.application.dto.CreateUserRequest;
import com.vladimir.tkach.myplanner.application.dto.UpdateUserRequest;
import com.vladimir.tkach.myplanner.application.dto.UserInfo;
import com.vladimir.tkach.myplanner.application.exception.EntityNotFoundException;
import com.vladimir.tkach.myplanner.application.exception.InvalidUserPasswordException;
import com.vladimir.tkach.myplanner.application.exception.SuchUserAlreadyExistsException;
import com.vladimir.tkach.myplanner.domain.user.User;
import com.vladimir.tkach.myplanner.infrastructure.repository.UserRepository;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;

@Service
@Validated
@AllArgsConstructor
public class UserService {

    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;
    private final ModelMapper modelMapper;

    public UserInfo getUser(String login) {
        User user = userRepository.findByLogin(login).orElseThrow(() ->
                new EntityNotFoundException(String.format("User with login=%s has not been found", login)));

        return modelMapper.map(user, UserInfo.class);
    }

    public UserInfo createUser(@Valid CreateUserRequest request) {
        if (userRepository.findByLogin(request.getLogin()).isPresent()) {
            throw new SuchUserAlreadyExistsException(String.format("User with login=%s already exists",
                    request.getLogin()));
        }

        String password = request.getPassword();
        if(password.length() > 70) {
            throw new InvalidUserPasswordException("max allowed password length is 70 characters");
        }

        String encodedPassword = passwordEncoder.encode(password);

        User user = new User(request.getLogin(),
                encodedPassword,
                request.getFirstName());

        User savedUser = userRepository.save(user);

        return modelMapper.map(savedUser, UserInfo.class);
    }

    public UserInfo updateUser(@Valid UpdateUserRequest request) {
        String currentLogin = request.getCurrentLogin();

        User user = userRepository.findByLogin(currentLogin).orElseThrow(() ->
                new EntityNotFoundException(String.format("User with login=%s has not been found", currentLogin)));

        String encodedPassword = passwordEncoder.encode(request.getPassword());

        user.setLogin(request.getNewLogin());
        user.setPassword(encodedPassword);
        user.setFirstName(request.getFirstName());

        User savedUser = userRepository.save(user);

        return modelMapper.map(savedUser, UserInfo.class);
    }
}
