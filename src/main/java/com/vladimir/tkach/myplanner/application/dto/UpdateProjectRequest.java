package com.vladimir.tkach.myplanner.application.dto;

import lombok.Builder;
import lombok.Data;
import lombok.NonNull;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.PositiveOrZero;

@Data
@Builder
public class UpdateProjectRequest {
    @NonNull
    private String ownerLogin;

    @NotNull(message = "'projectId' can't be null")
    @PositiveOrZero
    private Long projectId;

    @NotNull(message = "'name' can't be null")
    private String name;

    private String goal;

    @NotNull(message = "'priority' can't be null")
    private Integer priority;
}
