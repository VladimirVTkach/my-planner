package com.vladimir.tkach.myplanner.controller.rest;

import com.vladimir.tkach.myplanner.application.TaskService;
import com.vladimir.tkach.myplanner.application.dto.*;
import com.vladimir.tkach.myplanner.application.dto.GetProjectTasksRequest;
import com.vladimir.tkach.myplanner.controller.rest.dto.*;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.security.Principal;
import java.util.List;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
@AllArgsConstructor
public class TaskController {

    private final TaskService taskService;

    @GetMapping("/api/v1/tasks/{taskId}")
    public TaskInfo getTaskById(Principal principal,
                                @PathVariable Long taskId) {
        return taskService.getTaskById(GetTaskByIdRequest.builder()
                .userLogin(principal.getName())
                .taskId(taskId)
                .build());
    }

    @GetMapping("/api/v1/projects/{projectId}/tasks")
    public List<TaskInfo> getProjectTasks(Principal principal,
                                          @PathVariable Long projectId,
                                          GetProjectTasksControllerRequest request) {
        String userLogin = principal.getName();
        String taskTitle = request.getTitle();
        Integer limit = request.getLimit();
        Integer offset = request.getOffset();

        return taskService.getProjectTasks(GetProjectTasksRequest.builder()
                .userLogin(userLogin)
                .projectId(projectId)
                .title(taskTitle)
                .limit(limit)
                .offset(offset)
                .build());
    }

    @GetMapping("/api/v1/tasks")
    public List<TaskInfo> getAllTasks(Principal principal,
                                      GetAllTasksControllerRequest request) {
        return taskService.getAllTasks(GetAllTasksRequest.builder()
                .userLogin(principal.getName())
                .completed(request.getCompleted())
                .deadlineAfter(request.getDeadlineAfter())
                .limit(request.getLimit())
                .offset(request.getOffset())
                .build());
    }

    @PostMapping("/api/v1/projects/{projectId}/tasks")
    public ResponseEntity<TaskInfo> createTask(Principal principal,
                                               @PathVariable Long projectId,
                                               @RequestBody CreateTaskControllerRequest request) {
        TaskInfo task = taskService.createTask(CreateTaskRequest.builder()
                .userLogin(principal.getName())
                .projectId(projectId)
                .title(request.getTitle())
                .description(request.getDescription())
                .deadline(request.getDeadline())
                .priority(request.getPriority())
                .timeForCompletion(request.getTimeForCompletion())
                .timeForRest(request.getTimeForRest())
                .build());

        URI taskUri = linkTo(methodOn(TaskController.class)
                .getTaskById(null, task.getTaskId()))
                .toUri();

        return ResponseEntity.created(taskUri).body(task);
    }

    @PutMapping("/api/v1/tasks/{taskId}")
    public TaskInfo updateTask(Principal principal,
                               @PathVariable Long taskId,
                               @RequestBody UpdateTaskControllerRequest request) {
        return taskService.updateTask(UpdateTaskRequest.builder()
                .userLogin(principal.getName())
                .taskId(taskId)
                .title(request.getTitle())
                .description(request.getDescription())
                .deadline(request.getDeadline())
                .priority(request.getPriority())
                .timeForCompletion(request.getTimeForCompletion())
                .timeForRest(request.getTimeForRest())
                .completed(request.isCompleted())
                .build());
    }

    @PostMapping("/api/v1/tasks/{taskId}/subtasks")
    public List<TaskInfo> splitTask(Principal principal,
                                    @PathVariable Long taskId,
                                    @RequestBody SplitTaskControllerRequest request) {
        return taskService.splitTask(SplitTaskRequest.builder()
                .userLogin(principal.getName())
                .taskId(taskId)
                .subtasksCount(request.getSubtasksCount())
                .build());
    }

    @DeleteMapping("/api/v1/tasks/{taskId}")
    public ResponseEntity<Void> deleteTask(Principal principal,
                                           @PathVariable Long taskId) {
        taskService.deleteTask(DeleteTaskRequest.builder()
                .userLogin(principal.getName())
                .taskId(taskId)
                .build());

        return ResponseEntity.noContent().build();
    }
}
