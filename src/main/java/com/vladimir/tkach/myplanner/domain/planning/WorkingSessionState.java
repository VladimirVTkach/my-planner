package com.vladimir.tkach.myplanner.domain.planning;

public enum WorkingSessionState {
    CREATED,
    STARTED,
    FINISHED
}
