package com.vladimir.tkach.myplanner.controller.rest.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class UpdateProjectControllerRequest {
    private String name;
    private String goal;
    private Integer priority;
}
