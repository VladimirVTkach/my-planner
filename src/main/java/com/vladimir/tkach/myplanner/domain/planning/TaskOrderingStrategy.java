package com.vladimir.tkach.myplanner.domain.planning;

import java.util.List;

public interface TaskOrderingStrategy {
    List<Task> orderTasks(List<Task> tasks);
}
