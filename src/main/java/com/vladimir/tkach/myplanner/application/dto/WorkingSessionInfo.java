package com.vladimir.tkach.myplanner.application.dto;

import lombok.Builder;
import lombok.Data;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.List;

@Data
@Builder
public class WorkingSessionInfo {
    private Long workingSessionId;
    private String ownerLogin;
    private Duration specifiedDuration;
    private Duration actualDuration;
    private String state;
    private LocalDateTime startTime;
    private List<UnitOfWorkInfo> unitsOfWork;
}
