package com.vladimir.tkach.myplanner.controller.rest.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SplitTaskControllerRequest {
    private Integer subtasksCount;
}
