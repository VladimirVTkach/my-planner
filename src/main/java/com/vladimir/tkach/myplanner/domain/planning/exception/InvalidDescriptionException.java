package com.vladimir.tkach.myplanner.domain.planning.exception;

public class InvalidDescriptionException extends PlanningException {
    public InvalidDescriptionException(String message) {
        super(message);
    }
}
