package com.vladimir.tkach.myplanner.application.dto;

import lombok.Builder;
import lombok.Data;
import lombok.NonNull;

import javax.validation.constraints.NotNull;

@Data
@Builder
public class UpdateUserRequest {
    @NonNull
    private String currentLogin;

    @NotNull(message = "new login can't be null")
    private String newLogin;

    @NotNull(message = "password can't be null")
    private String password;

    @NotNull(message = "first name can't be null")
    private String firstName;
}
