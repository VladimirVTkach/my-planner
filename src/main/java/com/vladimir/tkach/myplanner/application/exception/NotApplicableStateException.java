package com.vladimir.tkach.myplanner.application.exception;

public class NotApplicableStateException extends RuntimeException {
    public NotApplicableStateException(String message) {
        super(message);
    }
}
