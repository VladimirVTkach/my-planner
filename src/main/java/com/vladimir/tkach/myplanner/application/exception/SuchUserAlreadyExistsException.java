package com.vladimir.tkach.myplanner.application.exception;

public class SuchUserAlreadyExistsException extends RuntimeException {
    public SuchUserAlreadyExistsException(String message) {
        super(message);
    }
}
