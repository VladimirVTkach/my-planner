package com.vladimir.tkach.myplanner.application.dto;

import lombok.Builder;
import lombok.Data;
import lombok.NonNull;

import javax.validation.constraints.NotNull;

@Data
@Builder
public class GetProjectByIdRequest {
    @NonNull
    private String ownerLogin;

    @NotNull(message = "project id can't be null")
    private Long projectId;
}
