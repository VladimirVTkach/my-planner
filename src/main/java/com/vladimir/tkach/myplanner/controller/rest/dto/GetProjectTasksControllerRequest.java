package com.vladimir.tkach.myplanner.controller.rest.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class GetProjectTasksControllerRequest {
    private String title;
    private Integer limit;
    private Integer offset;
}
