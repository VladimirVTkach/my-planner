package com.vladimir.tkach.myplanner.domain.planning;

import com.vladimir.tkach.myplanner.domain.user.Project;
import com.vladimir.tkach.myplanner.domain.user.User;
import com.vladimir.tkach.myplanner.domain.planning.exception.InvalidDeadlineException;
import com.vladimir.tkach.myplanner.domain.planning.exception.InvalidTaskSplitException;
import org.junit.jupiter.api.Test;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class TaskTest {
    @Test
    public void testIsPossibleToCompleteOnTime_whenItIsPossible() {
        User user = User.builder()
                .login("test_user")
                .password("test_password")
                .firstName("test_first_name")
                .build();

        Project project = Project.builder()
                .name("test_project")
                .goal("test_goal")
                .priority(1)
                .owner(user)
                .build();

        Task task = Task.builder()
                .project(project)
                .title("test")
                .description("test task")
                .deadline(LocalDateTime.now().plus(Duration.ofHours(10)))
                .priority(1)
                .timeForCompletion(Duration.ofMinutes(25))
                .timeForRest(Duration.ofMinutes(5))
                .build();

        assertTrue(task.isPossibleToCompleteOnTime());
    }

    @Test
    public void testTaskBuilder_whenItIsNotPossibleToCompleteTaskOnTime() {
        User user = User.builder()
                .login("test_user")
                .password("test_password")
                .firstName("test_first_name")
                .build();

        Project project = Project.builder()
                .name("test_project")
                .goal("test_goal")
                .priority(1)
                .owner(user)
                .build();

        assertThrows(InvalidDeadlineException.class, () -> Task.builder()
                .project(project)
                .title("test")
                .description("test task")
                .deadline(LocalDateTime.now().plus(Duration.ofMinutes(15)))
                .priority(1)
                .timeForCompletion(Duration.ofMinutes(25))
                .timeForRest(Duration.ofMinutes(5))
                .build());
    }

    @Test
    public void testGetTotalRequiredTime() {
        User user = User.builder()
                .login("test_user")
                .password("test_password")
                .firstName("test_first_name")
                .build();

        Project project = Project.builder()
                .name("test_project")
                .goal("test_goal")
                .priority(1)
                .owner(user)
                .build();

        Task task = Task.builder()
                .project(project)
                .title("test")
                .description("test task")
                .deadline(LocalDateTime.now().plus(Duration.ofHours(1)))
                .priority(1)
                .timeForCompletion(Duration.ofMinutes(25))
                .timeForRest(Duration.ofMinutes(5))
                .build();

        assertEquals(Duration.ofMinutes(30),
                task.getTotalRequiredTime());
    }

    @Test
    public void testSplitIntoSubtasks_whenSubtasksCountIsLessThanOne() {
        User user = User.builder()
                .login("test_user")
                .password("test_password")
                .firstName("test_first_name")
                .build();

        Project project = Project.builder()
                .name("test_project")
                .goal("test_goal")
                .priority(1)
                .owner(user)
                .build();

        Task task = Task.builder()
                .project(project)
                .title("test")
                .description("test task")
                .deadline(LocalDateTime.now().plus(Duration.ofHours(1)))
                .priority(1)
                .timeForCompletion(Duration.ofMinutes(25))
                .timeForRest(Duration.ofMinutes(5))
                .build();

        assertThrows(InvalidTaskSplitException.class,
                () -> task.splitIntoSubtasks(-3));
    }

    @Test
    public void testSplitIntoSubtasks_whenSubtasksCountEqualsToOne() {
        User user = User.builder()
                .login("test_user")
                .password("test_password")
                .firstName("test_first_name")
                .build();

        Project project = Project.builder()
                .name("test_project")
                .goal("test_goal")
                .priority(1)
                .owner(user)
                .build();

        Task task = Task.builder()
                .project(project)
                .title("test")
                .description("test task")
                .deadline(LocalDateTime.now().plus(Duration.ofHours(1)))
                .priority(1)
                .timeForCompletion(Duration.ofMinutes(25))
                .timeForRest(Duration.ofMinutes(5))
                .build();

        assertThrows(InvalidTaskSplitException.class,
                () -> task.splitIntoSubtasks(1));
    }

    @Test
    public void testSplitIntoSubtasks_whenSubtasksCountGreaterThanOneHundred() {
        User user = User.builder()
                .login("test_user")
                .password("test_password")
                .firstName("test_first_name")
                .build();

        Project project = Project.builder()
                .name("test_project")
                .goal("test_goal")
                .priority(1)
                .owner(user)
                .build();

        Task task = Task.builder()
                .project(project)
                .title("test")
                .description("test task")
                .deadline(LocalDateTime.now().plus(Duration.ofHours(1)))
                .priority(1)
                .timeForCompletion(Duration.ofMinutes(25))
                .timeForRest(Duration.ofMinutes(5))
                .build();

        assertThrows(InvalidTaskSplitException.class,
                () -> task.splitIntoSubtasks(300));
    }

    @Test
    public void testSplitIntoSubtasks_whenSubtasksCountIsValid() {
        User user = User.builder()
                .login("test_user")
                .password("test_password")
                .firstName("test_first_name")
                .build();

        Project project = Project.builder()
                .name("test_project")
                .goal("test_goal")
                .priority(1)
                .owner(user)
                .build();

        Task task = Task.builder()
                .project(project)
                .title("test")
                .description("test task")
                .deadline(LocalDateTime.now().plus(Duration.ofHours(1)))
                .priority(1)
                .timeForCompletion(Duration.ofMinutes(25))
                .timeForRest(Duration.ofMinutes(5))
                .build();

        List<Task> subtasks = task.splitIntoSubtasks(3);

        assertEquals(2, subtasks.size());
        assertEquals(subtasks.get(0).getTitle(), task.getTitle() + " (copy)");
        assertEquals(subtasks.get(1).getTitle(), task.getTitle() + " (copy)");
    }
}
