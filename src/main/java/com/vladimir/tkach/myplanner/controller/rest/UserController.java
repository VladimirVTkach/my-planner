package com.vladimir.tkach.myplanner.controller.rest;

import com.vladimir.tkach.myplanner.application.UserService;
import com.vladimir.tkach.myplanner.application.dto.CreateUserRequest;
import com.vladimir.tkach.myplanner.application.dto.UpdateUserRequest;
import com.vladimir.tkach.myplanner.application.dto.UserInfo;
import com.vladimir.tkach.myplanner.controller.rest.dto.UpdateUserControllerRequest;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.security.Principal;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
@AllArgsConstructor
public class UserController {

    private final UserService userService;

    @GetMapping("/api/v1/user")
    public UserInfo getUser(Principal principal) {
        return userService.getUser(principal.getName());
    }

    @PostMapping("/api/v1/users")
    public ResponseEntity<UserInfo> createUser(@RequestBody CreateUserRequest request) {
        UserInfo user = userService.createUser(request);

        URI userUri = linkTo(methodOn(UserController.class)
                .getUser(null))
                .toUri();

        return ResponseEntity.created(userUri).body(user);
    }

    @PutMapping("/api/v1/user")
    public UserInfo updateUser(Principal principal,
                               @RequestBody UpdateUserControllerRequest request) {
        return userService.updateUser(UpdateUserRequest.builder()
                .currentLogin(principal.getName())
                .newLogin(request.getLogin())
                .password(request.getPassword())
                .firstName(request.getFirstName())
                .build());
    }
}
