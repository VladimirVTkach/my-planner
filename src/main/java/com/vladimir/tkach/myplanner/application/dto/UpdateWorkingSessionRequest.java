package com.vladimir.tkach.myplanner.application.dto;

import lombok.Builder;
import lombok.Data;
import lombok.NonNull;

import javax.validation.constraints.NotNull;

@Data
@Builder
public class UpdateWorkingSessionRequest {
    @NonNull
    private String ownerLogin;

    @NotNull(message = "working session id can't be null")
    private Long workingSessionId;

    @NotNull(message = "working session state can't be null")
    private String workingSessionState;
}
