package com.vladimir.tkach.myplanner.domain.planning;

import com.vladimir.tkach.myplanner.domain.planning.exception.WorkingSessionCreationFailureException;
import com.vladimir.tkach.myplanner.domain.user.Project;
import com.vladimir.tkach.myplanner.domain.user.User;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

@SpringBootTest
public class WorkingSessionFactoryTest {

    @Autowired
    private WorkingSessionFactory workingSessionFactory;

    @MockBean
    private TaskOrderingStrategy taskOrderingStrategy;

    @Test
    public void testCreate_whenDurationIsLessThanAllowed() {
        User user = User.builder()
                .login("user")
                .password("password")
                .firstName("John")
                .build();

        Project project = Project.builder()
                .name("test_project")
                .goal("test_goal")
                .priority(1)
                .owner(user)
                .build();

        List<Task> tasks = List.of(Task.builder()
                        .project(project)
                        .title("test_task_1")
                        .description("test_description")
                        .deadline(LocalDateTime.now().plus(Duration.ofHours(1)))
                        .priority(1)
                        .timeForCompletion(Duration.ofMinutes(10))
                        .timeForRest(Duration.ofMinutes(3))
                        .build(),
                Task.builder()
                        .project(project)
                        .title("test_task_2")
                        .description("test_description")
                        .deadline(LocalDateTime.now().plus(Duration.ofHours(3)))
                        .priority(2)
                        .timeForCompletion(Duration.ofHours(1))
                        .timeForRest(Duration.ofMinutes(25))
                        .build());

        assertThrows(WorkingSessionCreationFailureException.class,
                () -> workingSessionFactory.create(user, Duration.ofMinutes(1), tasks));
    }

    @Test
    public void testCreate_whenThereAreNoTasks() {
        User user = User.builder()
                .login("user")
                .password("password")
                .firstName("John")
                .build();

        List<Task> tasks = new ArrayList<>();

        assertThrows(WorkingSessionCreationFailureException.class,
                () -> workingSessionFactory.create(user, Duration.ofMinutes(25), tasks));
    }

    @Test
    public void testCreate_whenThereAreNoSuitableTasksForSpecifiedDuration() {
        User user = User.builder()
                .login("user")
                .password("password")
                .firstName("John")
                .build();

        Project project = Project.builder()
                .name("test_project")
                .goal("test_goal")
                .priority(1)
                .owner(user)
                .build();

        Task firstTask = Task.builder()
                .project(project)
                .title("test_task_1")
                .description("test_description")
                .deadline(LocalDateTime.now().plus(Duration.ofHours(1)))
                .priority(1)
                .timeForCompletion(Duration.ofMinutes(45))
                .timeForRest(Duration.ofMinutes(10))
                .build();

        Task secondTask = Task.builder()
                .project(project)
                .title("test_task_2")
                .description("test_description")
                .deadline(LocalDateTime.now().plus(Duration.ofHours(3)))
                .priority(2)
                .timeForCompletion(Duration.ofHours(1))
                .timeForRest(Duration.ofMinutes(20))
                .build();

        List<Task> tasks = List.of(firstTask, secondTask);

        when(taskOrderingStrategy.orderTasks(tasks))
                .thenReturn(tasks);

        assertThrows(WorkingSessionCreationFailureException.class,
                () -> workingSessionFactory.create(user, Duration.ofMinutes(30), tasks));
    }

    @Test
    public void testCreate_whenDurationIsCorrectAndThereAreSomeTasks() {
        User user = User.builder()
                .login("user")
                .password("password")
                .firstName("John")
                .build();

        Project project = Project.builder()
                .name("test_project")
                .goal("test_goal")
                .priority(1)
                .owner(user)
                .build();

        Task firstTask = Task.builder()
                .project(project)
                .title("test_task_1")
                .description("test_description")
                .deadline(LocalDateTime.now().plus(Duration.ofHours(1)))
                .priority(1)
                .timeForCompletion(Duration.ofMinutes(10))
                .timeForRest(Duration.ofMinutes(3))
                .build();

        Task secondTask = Task.builder()
                .project(project)
                .title("test_task_2")
                .description("test_description")
                .deadline(LocalDateTime.now().plus(Duration.ofHours(3)))
                .priority(2)
                .timeForCompletion(Duration.ofHours(1))
                .timeForRest(Duration.ofMinutes(20))
                .build();

        List<Task> tasks = List.of(firstTask, secondTask);

        when(taskOrderingStrategy.orderTasks(tasks))
                .thenReturn(tasks);

        WorkingSession workingSession = workingSessionFactory.create(user,
                Duration.ofHours(2),
                tasks);

        List<UnitOfWork> unitsOfWork = workingSession.getUnitsOfWork();

        assertAll(() -> assertEquals(2, unitsOfWork.size()),
                () -> assertEquals(firstTask, unitsOfWork.get(0).getTask()),
                () -> assertEquals(secondTask, unitsOfWork.get(1).getTask()));
    }
}
