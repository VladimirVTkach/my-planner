package com.vladimir.tkach.myplanner.domain.planning;

import com.vladimir.tkach.myplanner.domain.planning.exception.DisallowedUnitOfWorkException;
import com.vladimir.tkach.myplanner.domain.planning.exception.InvalidStateTransitionException;
import com.vladimir.tkach.myplanner.domain.planning.exception.InvalidWorkingSessionDurationException;
import com.vladimir.tkach.myplanner.domain.user.User;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

import javax.persistence.*;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

@Entity
@Data
@NoArgsConstructor
public class WorkingSession {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long workingSessionId;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User owner;

    private LocalDateTime startTime;

    private Duration specifiedDuration;

    private Duration actualDuration;

    @Enumerated(value = EnumType.STRING)
    private WorkingSessionState state;

    @OneToMany(mappedBy = "workingSession",
            cascade = CascadeType.ALL,
            orphanRemoval = true,
            fetch = FetchType.EAGER)
    private List<UnitOfWork> unitsOfWork;

    public WorkingSession(@NonNull User owner,
                          @NonNull Duration specifiedDuration) {
        this.owner = owner;
        this.specifiedDuration = requireValidDuration(specifiedDuration);
        this.actualDuration = Duration.ZERO;
        this.state = WorkingSessionState.CREATED;
        this.unitsOfWork = new ArrayList<>();
    }

    public boolean isAllowedUnitOfWork(UnitOfWork unitOfWork) {
        Duration requiredTaskTime = unitOfWork.getTask().getTotalRequiredTime();
        return actualDuration.plus(requiredTaskTime).compareTo(specifiedDuration) <= 0;
    }

    public void addUnitOfWork(UnitOfWork unitOfWork) {
        if (isAllowedUnitOfWork(unitOfWork)) {
            Duration requiredTaskTime = unitOfWork.getTask().getTotalRequiredTime();
            actualDuration = actualDuration.plus(requiredTaskTime);

            unitOfWork.setWorkingSession(this);
            unitsOfWork.add(unitOfWork);
        } else {
            throw new DisallowedUnitOfWorkException("passed unit of work can't be added to the current working session");
        }
    }

    public void adjustDuration() {
        Duration durationRemainder = getDurationRemainder();

        Duration oneMinute = Duration.ofMinutes(1);
        if (durationRemainder.compareTo(oneMinute) >= 0) {
            List<UnitOfWork> sortedUnitsOfWork = new ArrayList<>(unitsOfWork);
            sortedUnitsOfWork.sort(Comparator.comparing(UnitOfWork::getTask,
                    Comparator.comparing(Task::getTimeForCompletion).reversed()));

            while (durationRemainder.compareTo(oneMinute) >= 0) {
                for (UnitOfWork unitOfWork : sortedUnitsOfWork) {
                    unitOfWork.getTask().increaseTimeForRest(oneMinute);
                    durationRemainder = durationRemainder.minus(oneMinute);
                    actualDuration = actualDuration.plus(oneMinute);

                    if (durationRemainder.compareTo(oneMinute) < 0) {
                        break;
                    }
                }
            }
        }
    }

    public boolean isStarted() {
        return state == WorkingSessionState.STARTED;
    }

    public boolean isFinished() {
        return state == WorkingSessionState.FINISHED;
    }

    public boolean hasTimeElapsed() {
        if (!isStarted()) {
            return false;
        }

        LocalDateTime currentTime = LocalDateTime.now();
        return currentTime.isAfter(startTime.plus(actualDuration));
    }

    public void start() {
        if (isFinished()) {
            throw new InvalidStateTransitionException(String.format("working session with id=%d can't be started, " +
                    "because it has been finished", workingSessionId));
        } else if (isStarted()) {
            throw new InvalidStateTransitionException(String.format("working session with id=%d has already been started",
                    workingSessionId));
        }

        state = WorkingSessionState.STARTED;
        startTime = LocalDateTime.now();

        LocalDateTime currentTime = startTime;
        for (UnitOfWork unitOfWork : unitsOfWork) {
            unitOfWork.setTimingIntervals(currentTime);
            currentTime = unitOfWork.getEndTime();
        }
    }

    public void finish() {
        if (!isStarted()) {
            throw new InvalidStateTransitionException(String.format("working session with id=%d can't be finished, " +
                    "because it hasn't been started", workingSessionId));
        }

        state = WorkingSessionState.FINISHED;
    }

    private Duration getDurationRemainder() {
        return specifiedDuration.minus(actualDuration);
    }

    private Duration requireValidDuration(Duration duration) {
        if (duration.isNegative() || duration.isZero()) {
            throw new InvalidWorkingSessionDurationException("duration must be positive");
        }

        return duration;
    }
}
