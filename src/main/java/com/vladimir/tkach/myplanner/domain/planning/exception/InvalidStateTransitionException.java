package com.vladimir.tkach.myplanner.domain.planning.exception;

public class InvalidStateTransitionException extends PlanningException {
    public InvalidStateTransitionException(String message) {
        super(message);
    }
}
