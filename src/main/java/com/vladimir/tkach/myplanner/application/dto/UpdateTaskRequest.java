package com.vladimir.tkach.myplanner.application.dto;

import lombok.Builder;
import lombok.Data;
import lombok.NonNull;

import javax.validation.constraints.NotNull;
import java.time.Duration;
import java.time.LocalDateTime;

@Data
@Builder
public class UpdateTaskRequest {
    @NonNull
    private String userLogin;

    @NotNull(message = "task id can't be null")
    private Long taskId;

    @NotNull(message = "task title can't be null")
    private String title;

    private String description;

    @NotNull(message = "deadline can't be null")
    private LocalDateTime deadline;

    @NotNull(message = "priority can't be null")
    private Integer priority;

    @NotNull(message = "time for completion can't be null")
    private Duration timeForCompletion;

    @NotNull(message = "time for rest can't be null")
    private Duration timeForRest;

    @NotNull(message = "completed can't be null")
    private boolean completed;
}
