package com.vladimir.tkach.myplanner.domain.planning.exception;

public class InvalidTimeForCompletionException extends PlanningException {
    public InvalidTimeForCompletionException(String message) {
        super(message);
    }
}
