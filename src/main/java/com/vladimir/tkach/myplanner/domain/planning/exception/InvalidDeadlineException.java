package com.vladimir.tkach.myplanner.domain.planning.exception;

public class InvalidDeadlineException extends PlanningException {
    public InvalidDeadlineException(String message) {
        super(message);
    }
}
