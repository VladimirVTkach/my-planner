package com.vladimir.tkach.myplanner.controller.rest;

import com.vladimir.tkach.myplanner.application.ProjectService;
import com.vladimir.tkach.myplanner.application.dto.*;
import com.vladimir.tkach.myplanner.controller.rest.dto.CreateProjectControllerRequest;
import com.vladimir.tkach.myplanner.controller.rest.dto.GetUserProjectsControllerRequest;
import com.vladimir.tkach.myplanner.controller.rest.dto.UpdateProjectControllerRequest;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.security.Principal;
import java.util.List;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
@AllArgsConstructor
public class ProjectController {

    private final ProjectService projectService;

    @GetMapping("/api/v1/projects/{projectId}")
    public ProjectInfo getProjectById(Principal principal,
                                      @PathVariable Long projectId) {
        return projectService.getProjectById(GetProjectByIdRequest.builder()
                .ownerLogin(principal.getName())
                .projectId(projectId)
                .build());
    }

    @GetMapping("/api/v1/projects")
    public List<ProjectInfo> getUserProjects(Principal principal,
                                             GetUserProjectsControllerRequest request) {
        String ownerLogin = principal.getName();
        String projectName = request.getName();
        Integer limit = request.getLimit();
        Integer offset = request.getOffset();

        return projectService.getUserProjects(GetUserProjectsRequest.builder()
                .ownerLogin(ownerLogin)
                .name(projectName)
                .limit(limit)
                .offset(offset)
                .build());
    }

    @PostMapping("/api/v1/projects")
    public ResponseEntity<ProjectInfo> createProject(Principal principal,
                                                     @RequestBody CreateProjectControllerRequest request) {
        ProjectInfo project = projectService.createProject(CreateProjectRequest.builder()
                .ownerLogin(principal.getName())
                .name(request.getName())
                .goal(request.getGoal())
                .priority(request.getPriority())
                .build());

        URI projectUri = linkTo(methodOn(ProjectController.class)
                .getProjectById(null, project.getProjectId()))
                .toUri();

        return ResponseEntity.created(projectUri).body(project);
    }

    @PutMapping("/api/v1/projects/{projectId}")
    public ProjectInfo updateProject(Principal principal,
                                     @PathVariable Long projectId,
                                     @RequestBody UpdateProjectControllerRequest request) {
        return projectService.updateProject(UpdateProjectRequest.builder()
                .ownerLogin(principal.getName())
                .projectId(projectId)
                .name(request.getName())
                .goal(request.getGoal())
                .priority(request.getPriority())
                .build());
    }

    @DeleteMapping("/api/v1/projects/{projectId}")
    public ResponseEntity<Void> deleteProject(Principal principal,
                                              @PathVariable Long projectId) {
        projectService.deleteProject(DeleteProjectRequest.builder()
                .ownerLogin(principal.getName())
                .projectId(projectId)
                .build());

        return ResponseEntity.noContent().build();
    }
}
