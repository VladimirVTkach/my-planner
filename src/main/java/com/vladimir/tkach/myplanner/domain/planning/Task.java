package com.vladimir.tkach.myplanner.domain.planning;

import com.vladimir.tkach.myplanner.domain.planning.exception.*;
import com.vladimir.tkach.myplanner.domain.user.Project;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

import javax.persistence.*;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
@Data
@NoArgsConstructor
public class Task {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long taskId;

    @ManyToOne
    @JoinColumn(name = "project_id")
    @NonNull
    private Project project;

    private String title;

    private String description;

    private LocalDateTime deadline;

    private Integer priority;

    private Duration timeForCompletion;

    private Duration timeForRest;

    private LocalDateTime creationTime;

    private LocalDateTime updateTime;

    private boolean completed;

    @Builder
    public Task(@NonNull Project project,
                @NonNull String title,
                String description,
                @NonNull LocalDateTime deadline,
                @NonNull Integer priority,
                @NonNull Duration timeForCompletion,
                Duration timeForRest) {
        this.project = project;
        this.title = requireValidTitle(title);
        this.description = requireValidDescription(description);
        this.priority = requireValidPriority(priority);
        this.timeForCompletion = requireValidTimeForCompletion(timeForCompletion);
        this.timeForRest = Objects.requireNonNullElse(requireValidTimeForRest(timeForRest), Duration.ZERO);
        this.deadline = requireValidDeadline(deadline, this.timeForCompletion, this.timeForRest);
    }

    public boolean isPossibleToCompleteOnTime() {
        return isValidDeadline(this.deadline, this.timeForCompletion, this.timeForRest);
    }

    public void increaseTimeForRest(Duration duration) {
        timeForRest = timeForRest.plus(duration);
    }

    public Duration getTotalRequiredTime() {
        return timeForCompletion.plus(timeForRest);
    }

    public List<Task> splitIntoSubtasks(int subtasksCount) {
        if (subtasksCount <= 1 || subtasksCount > 100) {
            throw new InvalidTaskSplitException("subtasks count must be greater than 1 and less than or equals to 100");
        }

        List<Task> subtasks = new ArrayList<>();
        String subtaskTitle = String.format("%s (copy)", title);
        for (int i = 0; i < subtasksCount - 1; i++) {
            subtasks.add(Task.builder()
                    .project(project)
                    .title(subtaskTitle)
                    .description(description)
                    .deadline(deadline)
                    .priority(priority)
                    .timeForCompletion(timeForCompletion)
                    .timeForRest(timeForRest)
                    .build());
        }

        return subtasks;
    }

    public void setTitle(String title) {
        this.title = requireValidTitle(title);
    }

    public void setDescription(String description) {
        this.description = requireValidDescription(description);
    }

    public void setDeadline(LocalDateTime deadline) {
        this.deadline = requireValidDeadline(deadline, timeForCompletion, timeForRest);
    }

    public void setPriority(Integer priority) {
        this.priority = requireValidPriority(priority);
    }

    public void setTimeForCompletion(Duration timeForCompletion) {
        this.timeForCompletion = requireValidTimeForCompletion(timeForCompletion);
    }

    public void setTimeForRest(Duration timeForRest) {
        this.timeForRest = Objects.requireNonNullElse(requireValidTimeForRest(timeForRest), Duration.ZERO);
    }

    @PrePersist
    private void setCreationTime() {
        this.creationTime = LocalDateTime.now();
    }

    @PreUpdate
    private void setUpdateTime() {
        this.updateTime = LocalDateTime.now();
    }

    private String requireValidTitle(String title) {
        if (title.isBlank()) {
            throw new InvalidTaskTitleException("title must be non-empty");
        }

        if(title.length() > 40) {
            throw new InvalidTaskTitleException("max allowed title length is 40 characters");
        }

        return title;
    }

    private String requireValidDescription(String description) {
        if (description != null && description.isBlank()) {
            throw new InvalidDescriptionException("description must be non-empty");
        }

        if(description != null && description.length() > 255) {
            throw new InvalidDescriptionException("max allowed description length is 255 characters");
        }

        return description;
    }

    private int requireValidPriority(int priority) {
        if (priority <= 0) {
            throw new InvalidTaskPriorityException("priority must be positive");
        }

        return priority;
    }

    private Duration requireValidTimeForCompletion(Duration timeForCompletion) {
        if (timeForCompletion.isNegative() || timeForCompletion.isZero()) {
            throw new InvalidTimeForCompletionException("time for completion must be positive");
        }

        return timeForCompletion;
    }

    private Duration requireValidTimeForRest(Duration timeForRest) {
        if (timeForRest != null && timeForRest.isNegative()) {
            throw new InvalidTimeForRestException("time for rest must be non-negative");
        }

        return timeForRest;
    }

    private LocalDateTime requireValidDeadline(LocalDateTime deadline,
                                               Duration timeForCompletion,
                                               Duration timeForRest) {
        if (!isValidDeadline(deadline, timeForCompletion, timeForRest)) {
            throw new InvalidDeadlineException("Task can't be created, " +
                    "because time needed for it's completion added to current date and time " +
                    "is after the specified deadline");
        }

        return deadline;
    }

    private boolean isValidDeadline(LocalDateTime deadline,
                                    Duration timeForCompletion,
                                    Duration timeForRest) {
        LocalDateTime currentDateTime = LocalDateTime.now();

        return deadline.isAfter(currentDateTime
                .plus(timeForCompletion)
                .plus(timeForRest));
    }
}
