package com.vladimir.tkach.myplanner.controller.rest.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.boot.convert.DurationFormat;
import org.springframework.boot.convert.DurationStyle;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.Duration;
import java.time.LocalDateTime;

@Data
@AllArgsConstructor
public class UpdateTaskControllerRequest {
    private String title;

    private String description;

    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private LocalDateTime deadline;

    private Integer priority;

    @DurationFormat(value = DurationStyle.ISO8601)
    private Duration timeForCompletion;

    @DurationFormat(value = DurationStyle.ISO8601)
    private Duration timeForRest;

    private boolean completed;
}
