package com.vladimir.tkach.myplanner.application.dto;

import lombok.Builder;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@Builder
public class UnitOfWorkInfo {
    private TaskInfo task;
    private LocalDateTime startTime;
    private LocalDateTime endTime;
}
