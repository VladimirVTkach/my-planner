package com.vladimir.tkach.myplanner.application;

import com.vladimir.tkach.myplanner.application.dto.*;
import com.vladimir.tkach.myplanner.application.exception.EntityNotFoundException;
import com.vladimir.tkach.myplanner.application.exception.ProjectWithSuchNameAlreadyExistsException;
import com.vladimir.tkach.myplanner.domain.user.Project;
import com.vladimir.tkach.myplanner.domain.user.User;
import com.vladimir.tkach.myplanner.infrastructure.repository.ProjectRepository;
import com.vladimir.tkach.myplanner.infrastructure.repository.UserRepository;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;
import java.util.List;
import java.util.Objects;

@Service
@Validated
@AllArgsConstructor
public class ProjectService {

    private final UserRepository userRepository;
    private final ProjectRepository projectRepository;
    private final ModelMapper modelMapper;

    public ProjectInfo getProjectById(@Valid GetProjectByIdRequest request) {
        Long projectId = request.getProjectId();

        Project project = projectRepository.findById(projectId).orElseThrow(() ->
                new EntityNotFoundException(String.format("Project with id=%d has not been found", projectId)));

        User owner = project.getOwner();
        if (!owner.getLogin().equals(request.getOwnerLogin())) {
            throw new EntityNotFoundException(String.format("Project with id=%d has not been found", projectId));
        }

        return modelMapper.map(project, ProjectInfo.class);
    }

    public List<ProjectInfo> getUserProjects(@Valid GetUserProjectsRequest request) {
        String ownerLogin = request.getOwnerLogin();
        String projectName = request.getName();
        int limit = Objects.requireNonNullElse(request.getLimit(), 10);
        int offset = Objects.requireNonNullElse(request.getOffset(), 0);

        User owner = userRepository.findByLogin(ownerLogin).orElseThrow(() ->
                new EntityNotFoundException(String.format("User with login=%s has not been found", ownerLogin)));

        Specification<Project> specification = getProjectsWithOwner(owner);

        if (projectName != null) {
            specification = specification.and(getProjectsWithName(projectName));
        }

        return projectRepository.findAll(specification, PageRequest.of(offset, limit))
                .stream()
                .map(project -> modelMapper.map(project, ProjectInfo.class))
                .toList();
    }

    public ProjectInfo createProject(@Valid CreateProjectRequest request) {
        String ownerLogin = request.getOwnerLogin();

        User owner = userRepository.findByLogin(ownerLogin).orElseThrow(() ->
                new EntityNotFoundException(String.format("User with login=%s has not been found", ownerLogin)));

        String projectName = request.getName();
        if (projectRepository.existsProjectByNameAndOwner(projectName, owner)) {
            throw new ProjectWithSuchNameAlreadyExistsException(String.format("Project with name='%s' already exists",
                    projectName));
        }

        Project project = Project.builder()
                .name(projectName)
                .goal(request.getGoal())
                .priority(request.getPriority())
                .owner(owner)
                .build();

        Project savedProject = projectRepository.save(project);

        return modelMapper.map(savedProject, ProjectInfo.class);
    }

    public ProjectInfo updateProject(@Valid UpdateProjectRequest request) {
        Long projectId = request.getProjectId();

        Project project = projectRepository.findById(projectId).orElseThrow(() ->
                new EntityNotFoundException(String.format("Project with id=%d has not been found", projectId)));

        User owner = project.getOwner();
        if (!owner.getLogin().equals(request.getOwnerLogin())) {
            throw new EntityNotFoundException(String.format("Project with id=%d has not been found", projectId));
        }

        String projectName = request.getName();
        if (!projectName.equals(project.getName()) && projectRepository.existsProjectByNameAndOwner(projectName, owner)) {
            throw new ProjectWithSuchNameAlreadyExistsException(String.format("Project with name='%s' already exists",
                    projectName));
        }

        project.setName(projectName);
        project.setGoal(request.getGoal());
        project.setPriority(request.getPriority());

        Project savedProject = projectRepository.save(project);

        return modelMapper.map(savedProject, ProjectInfo.class);
    }

    public void deleteProject(@Valid DeleteProjectRequest request) {
        Long projectId = request.getProjectId();

        Project project = projectRepository.findById(projectId).orElseThrow(() ->
                new EntityNotFoundException(String.format("Project with id=%d has not been found", projectId)));

        User owner = project.getOwner();
        if (!owner.getLogin().equals(request.getOwnerLogin())) {
            throw new EntityNotFoundException(String.format("Project with id=%d has not been found", projectId));
        }

        projectRepository.deleteById(projectId);
    }

    private Specification<Project> getProjectsWithOwner(User user) {
        return (root, criteriaQuery, criteriaBuilder) ->
                criteriaBuilder.equal(root.get("owner"), user);
    }

    private Specification<Project> getProjectsWithName(String name) {
        return (root, criteriaQuery, criteriaBuilder) ->
                criteriaBuilder.like(root.get("name"), "%" + name + "%");
    }
}
