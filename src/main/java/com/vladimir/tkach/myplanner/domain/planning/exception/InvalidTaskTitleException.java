package com.vladimir.tkach.myplanner.domain.planning.exception;

public class InvalidTaskTitleException extends PlanningException {
    public InvalidTaskTitleException(String message) {
        super(message);
    }
}
