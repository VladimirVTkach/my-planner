package com.vladimir.tkach.myplanner.domain.planning;

import com.vladimir.tkach.myplanner.domain.user.Project;
import com.vladimir.tkach.myplanner.domain.user.User;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.time.Duration;
import java.time.LocalDateTime;

public class UnitOfWorkTest {
    @Test
    public void testSetTimingIntervals() {
        User user = User.builder()
                .login("user_1")
                .password("password")
                .firstName("first_name")
                .build();

        Project project = Project.builder()
                .name("test_project")
                .goal("test_goal")
                .priority(1)
                .owner(user)
                .build();

        Task task = Task.builder()
                .project(project)
                .title("test_task_1")
                .description("test_description")
                .deadline(LocalDateTime.now().plus(Duration.ofHours(10)))
                .priority(1)
                .timeForCompletion(Duration.ofMinutes(45))
                .timeForRest(Duration.ofMinutes(5))
                .build();

        UnitOfWork unitOfWork = new UnitOfWork(task);

        LocalDateTime unitStartTime = LocalDateTime.now();

        unitOfWork.setTimingIntervals(unitStartTime);

        LocalDateTime expectedUnitEndTime = unitStartTime.plus(task.getTotalRequiredTime());

        Assertions.assertEquals(unitStartTime, unitOfWork.getStartTime());
        Assertions.assertEquals(expectedUnitEndTime, unitOfWork.getEndTime());
    }
}
