package com.vladimir.tkach.myplanner.controller.rest.advice;

import com.vladimir.tkach.myplanner.controller.rest.dto.ErrorResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.servlet.http.HttpServletRequest;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import java.time.LocalDateTime;
import java.util.Set;

@ControllerAdvice
public class ConstraintViolationExceptionControllerAdvice {
    @ExceptionHandler(ConstraintViolationException.class)
    public ResponseEntity<ErrorResponse> handle(ConstraintViolationException exception,
                                                HttpServletRequest request) {
        Set<ConstraintViolation<?>> constraintViolations = exception.getConstraintViolations();

        return ResponseEntity.unprocessableEntity()
                .body(ErrorResponse.builder()
                        .timestamp(LocalDateTime.now())
                        .status(HttpStatus.UNPROCESSABLE_ENTITY.value())
                        .error(constraintViolations.stream()
                                .findFirst()
                                .map(ConstraintViolation::getMessage)
                                .get())
                        .path(request.getRequestURI())
                        .build());
    }
}
