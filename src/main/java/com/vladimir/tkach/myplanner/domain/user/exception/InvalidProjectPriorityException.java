package com.vladimir.tkach.myplanner.domain.user.exception;

public class InvalidProjectPriorityException extends UserException {
    public InvalidProjectPriorityException(String message) {
        super(message);
    }
}
