package com.vladimir.tkach.myplanner.domain.planning;

import com.vladimir.tkach.myplanner.domain.planning.exception.WorkingSessionCreationFailureException;
import com.vladimir.tkach.myplanner.domain.user.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.time.Duration;
import java.util.List;

@Component
public class WorkingSessionFactory {

    private final TaskOrderingStrategy taskOrderingStrategy;
    private final Duration minAllowedWorkingSessionDuration;

    @Autowired
    public WorkingSessionFactory(TaskOrderingStrategy taskOrderingStrategy,
                                 @Value("${min-allowed-working-session-duration}") Duration minAllowedWorkingSessionDuration) {
        this.taskOrderingStrategy = taskOrderingStrategy;
        this.minAllowedWorkingSessionDuration = minAllowedWorkingSessionDuration;
    }

    public WorkingSession create(User user,
                                 Duration duration,
                                 List<Task> tasks) {
        if (duration.compareTo(minAllowedWorkingSessionDuration) < 0) {
            throw new WorkingSessionCreationFailureException(String.format("working session can't be created, " +
                            "because specified duration (%s) is below minimal allowed (%s)",
                    duration,
                    minAllowedWorkingSessionDuration));
        }

        if (tasks.isEmpty()) {
            throw new WorkingSessionCreationFailureException("working session can't be created, " +
                    "because tasks list is empty");
        }

        WorkingSession workingSession = new WorkingSession(user, duration);

        List<Task> orderedTasks = taskOrderingStrategy.orderTasks(tasks);
        for (Task task : orderedTasks) {
            UnitOfWork unitOfWork = new UnitOfWork(task);

            if (workingSession.isAllowedUnitOfWork(unitOfWork)) {
                workingSession.addUnitOfWork(unitOfWork);
            }
        }

        if (workingSession.getUnitsOfWork().isEmpty()) {
            throw new WorkingSessionCreationFailureException(String.format("working session can't be created, " +
                            "because there are no suitable tasks for the specified duration=%s",
                    duration));
        }

        return workingSession;
    }
}
