package com.vladimir.tkach.myplanner.application.dto;

import lombok.Builder;
import lombok.Data;
import lombok.NonNull;

import javax.validation.constraints.NotNull;

@Data
@Builder
public class GetTaskByIdRequest {
    @NonNull
    private String userLogin;

    @NotNull(message = "task id can't be null")
    private Long taskId;
}
