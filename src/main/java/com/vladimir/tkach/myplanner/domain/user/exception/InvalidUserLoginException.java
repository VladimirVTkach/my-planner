package com.vladimir.tkach.myplanner.domain.user.exception;

public class InvalidUserLoginException extends UserException {
    public InvalidUserLoginException(String message) {
        super(message);
    }
}
