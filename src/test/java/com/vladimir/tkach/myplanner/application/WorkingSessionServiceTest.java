package com.vladimir.tkach.myplanner.application;

import com.vladimir.tkach.myplanner.application.dto.*;
import com.vladimir.tkach.myplanner.application.exception.EntityNotFoundException;
import com.vladimir.tkach.myplanner.domain.planning.WorkingSession;
import com.vladimir.tkach.myplanner.domain.planning.WorkingSessionState;
import com.vladimir.tkach.myplanner.domain.planning.exception.WorkingSessionCreationFailureException;
import com.vladimir.tkach.myplanner.domain.user.Project;
import com.vladimir.tkach.myplanner.domain.planning.Task;
import com.vladimir.tkach.myplanner.domain.user.User;
import com.vladimir.tkach.myplanner.infrastructure.repository.TaskRepository;
import com.vladimir.tkach.myplanner.infrastructure.repository.UserRepository;
import com.vladimir.tkach.myplanner.infrastructure.repository.WorkingSessionRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

@SpringBootTest
@MockBean(TaskRepository.class)
public class WorkingSessionServiceTest {

    @Autowired
    private WorkingSessionService workingSessionService;

    @MockBean
    private UserRepository userRepository;

    @MockBean
    private TaskRepository taskRepository;

    @MockBean
    private WorkingSessionRepository workingSessionRepository;

    @Test
    public void testGetWorkingSession_whenWorkingSessionWithPassedIdDoesNotExist() {
        assertThrows(EntityNotFoundException.class, () -> workingSessionService.getWorkingSessionById(GetWorkingSessionByIdRequest.builder()
                .ownerLogin("user_1")
                .workingSessionId(1L)
                .build()));
    }

    @Test
    public void testGetWorkingSessionById_whenOwnerOfWorkingSessionIsOtherUser() {
        User user = User.builder()
                .login("user_1")
                .password("password")
                .firstName("first_name")
                .build();

        WorkingSession workingSession = new WorkingSession(user, Duration.ofMinutes(30));

        Long workingSessionId = 1L;

        when(workingSessionRepository.findById(workingSessionId))
                .thenReturn(Optional.of(workingSession));

        assertThrows(EntityNotFoundException.class, () -> workingSessionService.getWorkingSessionById(GetWorkingSessionByIdRequest.builder()
                .ownerLogin("user_2")
                .workingSessionId(workingSessionId)
                .build()));
    }

    @Test
    public void testGetWorkingSessionById_whenEverythingIsOk() {
        User user = User.builder()
                .login("user_1")
                .password("password")
                .firstName("first_name")
                .build();

        WorkingSession workingSession = new WorkingSession(user, Duration.ofMinutes(30));

        Long workingSessionId = 1L;

        when(workingSessionRepository.findById(workingSessionId))
                .thenReturn(Optional.of(workingSession));

        WorkingSessionInfo workingSessionInfo = workingSessionService.getWorkingSessionById(GetWorkingSessionByIdRequest.builder()
                .ownerLogin(user.getLogin())
                .workingSessionId(workingSessionId)
                .build());

        assertEquals(workingSession.getSpecifiedDuration(), workingSessionInfo.getSpecifiedDuration());
        assertEquals(workingSession.getState().name().toLowerCase(), workingSessionInfo.getState());
    }

    @Test
    public void testCreateWorkingSession_whenAnotherCreatedWorkingSessionAlreadyExists() {
        User user = User.builder()
                .login("test")
                .password("password")
                .firstName("John")
                .build();

        when(userRepository.findByLogin(user.getLogin()))
                .thenReturn(Optional.of(user));

        when(workingSessionRepository.existsWorkingSessionByOwnerAndState(user, WorkingSessionState.CREATED))
                .thenReturn(true);

        assertThrows(WorkingSessionCreationFailureException.class,
                () -> workingSessionService.createWorkingSession(CreateWorkingSessionRequest.builder()
                        .ownerLogin(user.getLogin())
                        .duration(Duration.ofMinutes(10))
                        .build()));
    }

    @Test
    public void testCreateWorkingSession_whenStartedWorkingSessionAlreadyExists() {
        User user = User.builder()
                .login("test")
                .password("password")
                .firstName("John")
                .build();

        when(userRepository.findByLogin(user.getLogin()))
                .thenReturn(Optional.of(user));

        when(workingSessionRepository.existsWorkingSessionByOwnerAndState(user, WorkingSessionState.STARTED))
                .thenReturn(true);

        assertThrows(WorkingSessionCreationFailureException.class,
                () -> workingSessionService.createWorkingSession(CreateWorkingSessionRequest.builder()
                        .ownerLogin(user.getLogin())
                        .duration(Duration.ofMinutes(10))
                        .build()));
    }

    @Test
    public void testCreateWorkingSession_whenEverythingIsOk() {
        User user = User.builder()
                .login("test")
                .password("password")
                .firstName("John")
                .build();

        Project project = Project.builder()
                .name("test_project")
                .goal("test_goal")
                .priority(1)
                .owner(user)
                .build();

        Task firstTask = Task.builder()
                .project(project)
                .title("test_task_1")
                .description("test_description_1")
                .deadline(LocalDateTime.now().plus(Duration.ofMinutes(30)))
                .priority(1)
                .timeForCompletion(Duration.ofMinutes(15))
                .timeForRest(Duration.ofMinutes(5))
                .build();

        Task secondTask = Task.builder()
                .project(project)
                .title("test_task_2")
                .description("test_description_2")
                .deadline(LocalDateTime.now().plus(Duration.ofHours(5)))
                .priority(3)
                .timeForCompletion(Duration.ofMinutes(30))
                .timeForRest(Duration.ofMinutes(15))
                .build();

        when(userRepository.findByLogin(user.getLogin()))
                .thenReturn(Optional.of(user));

        when(workingSessionRepository.existsWorkingSessionByOwnerAndState(user, WorkingSessionState.CREATED))
                .thenReturn(false);

        when(workingSessionRepository.existsWorkingSessionByOwnerAndState(user, WorkingSessionState.STARTED))
                .thenReturn(false);

        when(taskRepository.findAllByProjectOwnerAndCompletedAndDeadlineAfter(eq(user), eq(false), any()))
                .thenReturn(List.of(firstTask, secondTask));

        when(workingSessionRepository.save(any()))
                .thenAnswer(invocationOnMock -> invocationOnMock.getArgument(0));

        CreateWorkingSessionRequest request = CreateWorkingSessionRequest.builder()
                .ownerLogin(user.getLogin())
                .duration(Duration.ofMinutes(65))
                .build();

        WorkingSessionInfo workingSessionInfo = workingSessionService.createWorkingSession(request);
        TaskInfo createdWorkingSessionFirstTask = workingSessionInfo.getUnitsOfWork().get(0).getTask();
        TaskInfo createdWorkingSessionSecondTask = workingSessionInfo.getUnitsOfWork().get(1).getTask();

        assertEquals(request.getDuration(), workingSessionInfo.getSpecifiedDuration());
        assertEquals("created", workingSessionInfo.getState());
        assertEquals(2, workingSessionInfo.getUnitsOfWork().size());

        assertEquals(firstTask.getTitle(), createdWorkingSessionFirstTask.getTitle());
        assertEquals(firstTask.getDescription(), createdWorkingSessionFirstTask.getDescription());
        assertEquals(firstTask.getDeadline(), createdWorkingSessionFirstTask.getDeadline());
        assertEquals(firstTask.getPriority(), createdWorkingSessionFirstTask.getPriority());
        assertEquals(firstTask.getTimeForCompletion(), createdWorkingSessionFirstTask.getTimeForCompletion());
        assertEquals(firstTask.getTimeForRest(), createdWorkingSessionFirstTask.getTimeForRest());

        assertEquals(secondTask.getTitle(), createdWorkingSessionSecondTask.getTitle());
        assertEquals(secondTask.getDescription(), createdWorkingSessionSecondTask.getDescription());
        assertEquals(secondTask.getDeadline(), createdWorkingSessionSecondTask.getDeadline());
        assertEquals(secondTask.getPriority(), createdWorkingSessionSecondTask.getPriority());
        assertEquals(secondTask.getTimeForCompletion(), createdWorkingSessionSecondTask.getTimeForCompletion());
        assertEquals(secondTask.getTimeForRest(), createdWorkingSessionSecondTask.getTimeForRest());
    }

    @Test
    public void testUpdateWorkingSession_whenWorkingSessionWithPassedIdDoesNotExist() {
        assertThrows(EntityNotFoundException.class, () -> workingSessionService.updateWorkingSession(UpdateWorkingSessionRequest.builder()
                .ownerLogin("user_1")
                .workingSessionId(1L)
                .workingSessionState("started")
                .build()));
    }

    @Test
    public void testUpdatedWorkingSession_whenOwnerOfWorkingSessionIsOtherUser() {
        User user = User.builder()
                .login("user_1")
                .password("password")
                .firstName("first_name")
                .build();

        WorkingSession workingSession = new WorkingSession(user, Duration.ofMinutes(30));

        Long workingSessionId = 1L;

        when(workingSessionRepository.findById(workingSessionId))
                .thenReturn(Optional.of(workingSession));

        assertThrows(EntityNotFoundException.class, () -> workingSessionService.updateWorkingSession(UpdateWorkingSessionRequest.builder()
                .ownerLogin("user_2")
                .workingSessionId(workingSessionId)
                .workingSessionState("started")
                .build()));
    }

    @Test
    public void testUpdateWorkingSession_whenEverythingIsOk() {
        User user = User.builder()
                .login("user_1")
                .password("password")
                .firstName("first_name")
                .build();

        WorkingSession workingSession = new WorkingSession(user, Duration.ofMinutes(30));

        Long workingSessionId = 1L;

        when(workingSessionRepository.findById(workingSessionId))
                .thenReturn(Optional.of(workingSession));

        when(workingSessionRepository.save(workingSession))
                .thenAnswer(invocationOnMock -> invocationOnMock.getArgument(0));

        UpdateWorkingSessionRequest request = UpdateWorkingSessionRequest.builder()
                .ownerLogin(user.getLogin())
                .workingSessionId(workingSessionId)
                .workingSessionState("started")
                .build();

        WorkingSessionInfo workingSessionInfo = workingSessionService.updateWorkingSession(request);

        assertEquals(workingSession.getSpecifiedDuration(), workingSessionInfo.getSpecifiedDuration());
        assertEquals(request.getWorkingSessionState(), workingSessionInfo.getState());
        assertNotNull(workingSession.getStartTime());
    }

    @Test
    public void testDeleteWorkingSession_whenWorkingSessionWithPassedIdDoesNotExist() {
        assertThrows(EntityNotFoundException.class, () -> workingSessionService.deleteWorkingSession(DeleteWorkingSessionRequest.builder()
                .ownerLogin("user_1")
                .workingSessionId(1L)
                .build()));
    }

    @Test
    public void testDeleteWorkingSession_whenOwnerOfWorkingSessionIsOtherUser() {
        User user = User.builder()
                .login("user_1")
                .password("password")
                .firstName("first_name")
                .build();

        WorkingSession workingSession = new WorkingSession(user, Duration.ofMinutes(30));

        Long workingSessionId = 1L;

        when(workingSessionRepository.findById(workingSessionId))
                .thenReturn(Optional.of(workingSession));

        assertThrows(EntityNotFoundException.class, () -> workingSessionService.deleteWorkingSession(DeleteWorkingSessionRequest.builder()
                .ownerLogin("user_2")
                .workingSessionId(workingSessionId)
                .build()));
    }

    @Test
    public void testDeleteWorkingSession_whenEverythingIsOk() {
        User user = User.builder()
                .login("user_1")
                .password("password")
                .firstName("first_name")
                .build();

        WorkingSession workingSession = new WorkingSession(user, Duration.ofMinutes(30));

        Long workingSessionId = 1L;

        when(workingSessionRepository.findById(workingSessionId))
                .thenReturn(Optional.of(workingSession));

        workingSessionService.deleteWorkingSession(DeleteWorkingSessionRequest.builder()
                .ownerLogin(user.getLogin())
                .workingSessionId(workingSessionId)
                .build());

        verify(workingSessionRepository, times(1)).deleteById(workingSessionId);
    }
}
