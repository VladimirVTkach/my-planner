package com.vladimir.tkach.myplanner.application.dto;

import lombok.Builder;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
@Builder
public class CreateUserRequest {
    @NotNull(message = "login can't be null")
    private String login;

    @NotNull(message = "password can't be null")
    private String password;

    @NotNull(message = "first name can't be null")
    private String firstName;
}
