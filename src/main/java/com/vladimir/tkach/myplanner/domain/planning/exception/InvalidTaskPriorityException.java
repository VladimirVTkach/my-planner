package com.vladimir.tkach.myplanner.domain.planning.exception;

public class InvalidTaskPriorityException extends PlanningException {
    public InvalidTaskPriorityException(String message) {
        super(message);
    }
}
