package com.vladimir.tkach.myplanner.domain.planning.exception;

public class WorkingSessionCreationFailureException extends PlanningException {
    public WorkingSessionCreationFailureException(String message) {
        super(message);
    }
}
