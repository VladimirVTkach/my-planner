package com.vladimir.tkach.myplanner.domain.planning;

import com.vladimir.tkach.myplanner.domain.user.Project;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

@Component
public class BaseTaskOrderingStrategy implements TaskOrderingStrategy {
    @Override
    public List<Task> orderTasks(List<Task> tasks) {
        List<Task> orderedTasks = new ArrayList<>(tasks);

        Comparator<Project> projectComparator =
                Comparator.comparingInt(Project::getPriority);

        Comparator<Task> taskComparator = Comparator.comparing(Task::getDeadline)
                .thenComparing(Task::getProject, projectComparator)
                .thenComparing(Task::getPriority)
                .thenComparing(Task::getTimeForCompletion);

        orderedTasks.sort(taskComparator);
        return orderedTasks;
    }
}
