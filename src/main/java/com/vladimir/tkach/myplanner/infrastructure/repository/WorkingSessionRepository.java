package com.vladimir.tkach.myplanner.infrastructure.repository;

import com.vladimir.tkach.myplanner.domain.planning.WorkingSession;
import com.vladimir.tkach.myplanner.domain.planning.WorkingSessionState;
import com.vladimir.tkach.myplanner.domain.user.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;

public interface WorkingSessionRepository extends JpaRepository<WorkingSession, Long>, JpaSpecificationExecutor<WorkingSession> {
    boolean existsWorkingSessionByOwnerAndState(User owner,
                                                WorkingSessionState state);
    List<WorkingSession> findAllByState(WorkingSessionState state);
}
