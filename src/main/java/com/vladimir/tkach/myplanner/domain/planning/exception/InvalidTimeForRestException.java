package com.vladimir.tkach.myplanner.domain.planning.exception;

public class InvalidTimeForRestException extends PlanningException {
    public InvalidTimeForRestException(String message) {
        super(message);
    }
}
