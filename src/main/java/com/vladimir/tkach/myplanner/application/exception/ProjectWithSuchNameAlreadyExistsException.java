package com.vladimir.tkach.myplanner.application.exception;

public class ProjectWithSuchNameAlreadyExistsException extends RuntimeException {
    public ProjectWithSuchNameAlreadyExistsException(String message) {
        super(message);
    }
}
