package com.vladimir.tkach.myplanner.infrastructure.repository;

import com.vladimir.tkach.myplanner.domain.user.Project;
import com.vladimir.tkach.myplanner.domain.user.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface ProjectRepository extends JpaRepository<Project, Long>, JpaSpecificationExecutor<Project> {
    boolean existsProjectByNameAndOwner(String name, User owner);
}
