package com.vladimir.tkach.myplanner.controller.rest.advice;

import com.vladimir.tkach.myplanner.controller.rest.dto.ErrorResponse;
import com.vladimir.tkach.myplanner.domain.planning.exception.PlanningException;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.servlet.http.HttpServletRequest;

@AllArgsConstructor
@ControllerAdvice
public class PlanningExceptionControllerAdvice {

    private final GenericControllerAdvice genericControllerAdvice;

    @ExceptionHandler(PlanningException.class)
    public ResponseEntity<ErrorResponse> handle(PlanningException exception,
                                                HttpServletRequest request) {
        return genericControllerAdvice.handle(exception, request, HttpStatus.UNPROCESSABLE_ENTITY);
    }
}
