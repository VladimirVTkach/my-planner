package com.vladimir.tkach.myplanner.domain.planning.exception;

public class InvalidWorkingSessionDurationException extends PlanningException {
    public InvalidWorkingSessionDurationException(String message) {
        super(message);
    }
}


