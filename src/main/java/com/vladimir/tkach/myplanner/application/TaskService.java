package com.vladimir.tkach.myplanner.application;

import com.vladimir.tkach.myplanner.application.dto.*;
import com.vladimir.tkach.myplanner.application.exception.EntityNotFoundException;
import com.vladimir.tkach.myplanner.domain.planning.Task;
import com.vladimir.tkach.myplanner.domain.planning.TaskOrderingStrategy;
import com.vladimir.tkach.myplanner.domain.user.Project;
import com.vladimir.tkach.myplanner.domain.user.User;
import com.vladimir.tkach.myplanner.infrastructure.repository.ProjectRepository;
import com.vladimir.tkach.myplanner.infrastructure.repository.TaskRepository;
import com.vladimir.tkach.myplanner.infrastructure.repository.UserRepository;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;
import java.util.List;
import java.util.Objects;

@Service
@Validated
@AllArgsConstructor
public class TaskService {

    private final ProjectRepository projectRepository;
    private final TaskRepository taskRepository;
    private final UserRepository userRepository;
    private final TaskOrderingStrategy taskOrderingStrategy;
    private final ModelMapper modelMapper;

    public TaskInfo getTaskById(@Valid GetTaskByIdRequest request) {
        Long taskId = request.getTaskId();

        Task task = taskRepository.findById(taskId).orElseThrow(() ->
                new EntityNotFoundException(String.format("Task with id=%d has not been found.", taskId)));

        Project project = task.getProject();
        User projectOwner = project.getOwner();
        if (!projectOwner.getLogin().equals(request.getUserLogin())) {
            throw new EntityNotFoundException(String.format("Task with id=%d has not been found.", taskId));
        }

        return modelMapper.map(task, TaskInfo.class);
    }

    public List<TaskInfo> getProjectTasks(@Valid GetProjectTasksRequest request) {
        Long projectId = request.getProjectId();
        String title = request.getTitle();
        int limit = Objects.requireNonNullElse(request.getLimit(), 10);
        int offset = Objects.requireNonNullElse(request.getOffset(), 0);

        Project project = projectRepository.findById(projectId).orElseThrow(() ->
                new EntityNotFoundException(String.format("Project with id=%d has not been found", projectId)));

        User projectOwner = project.getOwner();
        if (!projectOwner.getLogin().equals(request.getUserLogin())) {
            throw new EntityNotFoundException(String.format("Project with id=%d has not been found.", projectId));
        }

        Specification<Task> specification = getTasksWithProject(project);

        if (title != null) {
            specification = specification.and(getTasksWithTitle(title));
        }

        return taskRepository.findAll(specification, PageRequest.of(offset, limit))
                .stream()
                .map(task -> modelMapper.map(task, TaskInfo.class))
                .toList();
    }

    public List<TaskInfo> getAllTasks(@Valid GetAllTasksRequest request) {
        String userLogin = request.getUserLogin();

        User user = userRepository.findByLogin(userLogin).orElseThrow(() ->
                new EntityNotFoundException(String.format("User with login=%s has not been found", userLogin)));

        List<Task> tasks = taskRepository.findAllByProjectOwnerAndCompletedAndDeadlineAfter(user,
                request.getCompleted(),
                request.getDeadlineAfter());

        List<Task> orderedTasks = taskOrderingStrategy.orderTasks(tasks);

        int limit = request.getLimit();
        int offset = request.getOffset();

        return orderedTasks.stream()
                .skip((long) offset * limit)
                .limit(limit)
                .map(task -> modelMapper.map(task, TaskInfo.class))
                .toList();
    }

    public TaskInfo createTask(@Valid CreateTaskRequest request) {
        Long projectId = request.getProjectId();

        Project project = projectRepository.findById(projectId).orElseThrow(() ->
                new EntityNotFoundException(String.format("Project with id=%d has not been found", projectId)));

        User owner = project.getOwner();
        if (!owner.getLogin().equals(request.getUserLogin())) {
            throw new EntityNotFoundException(String.format("Project with id=%d has not been found", projectId));
        }

        Task task = Task.builder()
                .project(project)
                .title(request.getTitle())
                .description(request.getDescription())
                .deadline(request.getDeadline())
                .priority(request.getPriority())
                .timeForCompletion(request.getTimeForCompletion())
                .timeForRest(request.getTimeForRest())
                .build();

        Task savedTask = taskRepository.save(task);

        return modelMapper.map(savedTask, TaskInfo.class);
    }

    public TaskInfo updateTask(@Valid UpdateTaskRequest request) {
        Long taskId = request.getTaskId();

        Task task = taskRepository.findById(taskId).orElseThrow(() ->
                new EntityNotFoundException(String.format("Task with id=%d has not been found.", taskId)));

        Project project = task.getProject();
        User projectOwner = project.getOwner();

        if (!projectOwner.getLogin().equals(request.getUserLogin())) {
            throw new EntityNotFoundException(String.format("Task with id=%d has not been found", taskId));
        }

        task.setTitle(request.getTitle());
        task.setDescription(request.getDescription());
        task.setDeadline(request.getDeadline());
        task.setPriority(request.getPriority());
        task.setTimeForCompletion(request.getTimeForCompletion());
        task.setTimeForRest(request.getTimeForRest());
        task.setCompleted(request.isCompleted());

        Task savedTask = taskRepository.save(task);

        return modelMapper.map(savedTask, TaskInfo.class);
    }

    public List<TaskInfo> splitTask(@Valid SplitTaskRequest request) {
        Long taskId = request.getTaskId();

        Task task = taskRepository.findById(taskId).orElseThrow(() ->
                new EntityNotFoundException(String.format("Task with id=%d has not been found.", taskId)));

        Project project = task.getProject();
        User projectOwner = project.getOwner();

        if (!projectOwner.getLogin().equals(request.getUserLogin())) {
            throw new EntityNotFoundException(String.format("Task with id=%d has not been found", taskId));
        }

        List<Task> subtasks = task.splitIntoSubtasks(request.getSubtasksCount());
        List<Task> savedSubtasks = taskRepository.saveAll(subtasks);

        return savedSubtasks.stream()
                .map(subtask -> modelMapper.map(subtask, TaskInfo.class))
                .toList();
    }

    public void deleteTask(@Valid DeleteTaskRequest request) {
        Long taskId = request.getTaskId();

        Task task = taskRepository.findById(taskId).orElseThrow(() ->
                new EntityNotFoundException(String.format("Task with id=%d has not been found.", taskId)));

        Project project = task.getProject();
        User projectOwner = project.getOwner();

        if (!projectOwner.getLogin().equals(request.getUserLogin())) {
            throw new EntityNotFoundException(String.format("Task with id=%d has not been found", taskId));
        }

        taskRepository.deleteById(taskId);
    }

    private Specification<Task> getTasksWithProject(Project project) {
        return (root, criteriaQuery, criteriaBuilder) ->
                criteriaBuilder.equal(root.get("project"), project);
    }

    private Specification<Task> getTasksWithTitle(String title) {
        return (root, criteriaQuery, criteriaBuilder) ->
                criteriaBuilder.like(root.get("title"), "%" + title + "%");
    }
}
