package com.vladimir.tkach.myplanner.controller.rest;

import com.vladimir.tkach.myplanner.application.WorkingSessionService;
import com.vladimir.tkach.myplanner.application.dto.*;
import com.vladimir.tkach.myplanner.controller.rest.dto.CreateWorkingSessionControllerRequest;
import com.vladimir.tkach.myplanner.controller.rest.dto.GetUserWorkingSessionsControllerRequest;
import com.vladimir.tkach.myplanner.controller.rest.dto.UpdateWorkingSessionControllerRequest;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.security.Principal;
import java.util.List;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
@AllArgsConstructor
public class WorkingSessionController {

    private final WorkingSessionService workingSessionService;

    @GetMapping("/api/v1/working-sessions/{workingSessionId}")
    public WorkingSessionInfo getWorkingSessionById(Principal principal,
                                                    @PathVariable Long workingSessionId) {
        return workingSessionService.getWorkingSessionById(GetWorkingSessionByIdRequest.builder()
                .ownerLogin(principal.getName())
                .workingSessionId(workingSessionId)
                .build());
    }

    @GetMapping("/api/v1/working-sessions")
    public List<WorkingSessionInfo> getUserWorkingSessions(Principal principal,
                                                           GetUserWorkingSessionsControllerRequest request) {
        return workingSessionService.getUserWorkingSessions(GetUserWorkingSessionsRequest.builder()
                .userLogin(principal.getName())
                .state(request.getState())
                .startTime(request.getStartTime())
                .limit(request.getLimit())
                .offset(request.getOffset())
                .build());
    }

    @PostMapping("/api/v1/working-sessions")
    public ResponseEntity<WorkingSessionInfo> createWorkingSession(Principal principal,
                                                                   @RequestBody CreateWorkingSessionControllerRequest request) {
        WorkingSessionInfo workingSession = workingSessionService.createWorkingSession(CreateWorkingSessionRequest.builder()
                .ownerLogin(principal.getName())
                .duration(request.getDuration())
                .build());

        URI workingSessionUri = linkTo(methodOn(WorkingSessionController.class)
                .getWorkingSessionById(null, workingSession.getWorkingSessionId()))
                .toUri();

        return ResponseEntity.created(workingSessionUri).body(workingSession);
    }

    @PatchMapping("/api/v1/working-sessions/{workingSessionId}")
    public WorkingSessionInfo updateWorkingSession(Principal principal,
                                                   @PathVariable Long workingSessionId,
                                                   @RequestBody UpdateWorkingSessionControllerRequest request) {
        return workingSessionService.updateWorkingSession(UpdateWorkingSessionRequest.builder()
                .ownerLogin(principal.getName())
                .workingSessionId(workingSessionId)
                .workingSessionState(request.getState())
                .build());
    }

    @DeleteMapping("/api/v1/working-sessions/{workingSessionId}")
    public ResponseEntity<Void> deleteWorkingSession(Principal principal,
                                                     @PathVariable Long workingSessionId) {
        workingSessionService.deleteWorkingSession(DeleteWorkingSessionRequest.builder()
                .ownerLogin(principal.getName())
                .workingSessionId(workingSessionId)
                .build());

        return ResponseEntity.noContent().build();
    }
}
