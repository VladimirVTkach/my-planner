package com.vladimir.tkach.myplanner.application;

import com.vladimir.tkach.myplanner.application.dto.*;
import com.vladimir.tkach.myplanner.application.exception.EntityNotFoundException;
import com.vladimir.tkach.myplanner.application.exception.NotApplicableStateException;
import com.vladimir.tkach.myplanner.application.exception.WorkingSessionStateParsingException;
import com.vladimir.tkach.myplanner.domain.planning.Task;
import com.vladimir.tkach.myplanner.domain.planning.WorkingSession;
import com.vladimir.tkach.myplanner.domain.planning.WorkingSessionFactory;
import com.vladimir.tkach.myplanner.domain.planning.WorkingSessionState;
import com.vladimir.tkach.myplanner.domain.planning.exception.WorkingSessionCreationFailureException;
import com.vladimir.tkach.myplanner.domain.user.User;
import com.vladimir.tkach.myplanner.infrastructure.repository.TaskRepository;
import com.vladimir.tkach.myplanner.infrastructure.repository.UserRepository;
import com.vladimir.tkach.myplanner.infrastructure.repository.WorkingSessionRepository;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@Validated
@AllArgsConstructor
public class WorkingSessionService {

    private final WorkingSessionFactory workingSessionFactory;
    private final UserRepository userRepository;
    private final TaskRepository taskRepository;
    private final WorkingSessionRepository workingSessionRepository;
    private final ModelMapper modelMapper;

    public WorkingSessionInfo getWorkingSessionById(@Valid GetWorkingSessionByIdRequest request) {
        Long workingSessionId = request.getWorkingSessionId();

        WorkingSession workingSession = workingSessionRepository.findById(workingSessionId).orElseThrow(() ->
                new EntityNotFoundException(String.format("Working session with id=%d has not been found", workingSessionId)));

        User owner = workingSession.getOwner();
        if (!owner.getLogin().equals(request.getOwnerLogin())) {
            throw new EntityNotFoundException(String.format("Working session with id=%d has not been found", workingSessionId));
        }

        return map(workingSession);
    }

    public List<WorkingSessionInfo> getUserWorkingSessions(@Valid GetUserWorkingSessionsRequest request) {
        String userLogin = request.getUserLogin();
        WorkingSessionState state = requireValidWorkingSessionStateOrNull(request.getState());
        LocalDateTime startTime = request.getStartTime();

        int limit = Objects.requireNonNullElse(request.getLimit(), 5);
        int offset = Objects.requireNonNullElse(request.getOffset(), 0);

        User user = userRepository.findByLogin(userLogin).orElseThrow(() ->
                new EntityNotFoundException(String.format("User with login=%s has not been found", userLogin)));

        Specification<WorkingSession> specification = getWorkingSessionsWithOwner(user);

        if (state != null) {
            specification = specification.and(getWorkingSessionsWithState(state));
        }

        if (startTime != null) {
            specification = specification.and(getWorkingSessionsWithStartTime(startTime));
        }

        return workingSessionRepository.findAll(specification, PageRequest.of(offset, limit))
                .stream()
                .map(this::map)
                .toList();
    }

    public WorkingSessionInfo createWorkingSession(@Valid CreateWorkingSessionRequest request) {
        String userLogin = request.getOwnerLogin();

        User user = userRepository.findByLogin(userLogin).orElseThrow(() ->
                new EntityNotFoundException(String.format("User with login=%s has not been found", userLogin)));

        if (workingSessionRepository.existsWorkingSessionByOwnerAndState(user, WorkingSessionState.CREATED) ||
                workingSessionRepository.existsWorkingSessionByOwnerAndState(user, WorkingSessionState.STARTED)) {
            throw new WorkingSessionCreationFailureException(String.format("New working session for the user " +
                            "with id=%d can't be created, because there is at least one working session " +
                            "in 'created' or 'started' state already exists",
                    user.getUserId()));
        }

        LocalDateTime currentDateTime = LocalDateTime.now();
        List<Task> uncompletedTasks = taskRepository.findAllByProjectOwnerAndCompletedAndDeadlineAfter(user, false, currentDateTime)
                .stream()
                .filter(Task::isPossibleToCompleteOnTime)
                .toList();

        WorkingSession workingSession = workingSessionFactory.create(user,
                request.getDuration(),
                uncompletedTasks);

        WorkingSession savedWorkingSession = workingSessionRepository.save(workingSession);

        return map(savedWorkingSession);
    }

    public WorkingSessionInfo updateWorkingSession(@Valid UpdateWorkingSessionRequest request) {
        String ownerLogin = request.getOwnerLogin();
        Long workingSessionId = request.getWorkingSessionId();
        WorkingSessionState state = requireValidWorkingSessionStateOrNull(request.getWorkingSessionState());

        WorkingSession workingSession = workingSessionRepository.findById(workingSessionId).orElseThrow(() ->
                new EntityNotFoundException(String.format("Working session with id=%d has not been found", workingSessionId)));

        User owner = workingSession.getOwner();
        if (!owner.getLogin().equals(ownerLogin)) {
            throw new EntityNotFoundException(String.format("Working session with id=%d has not been found", workingSessionId));
        }

        switch (state) {
            case CREATED -> throw new NotApplicableStateException("State 'created' can't be applied for update");
            case STARTED -> workingSession.start();
            case FINISHED -> workingSession.finish();
        }

        WorkingSession savedWorkingSession = workingSessionRepository.save(workingSession);
        return map(savedWorkingSession);
    }

    public void deleteWorkingSession(@Valid DeleteWorkingSessionRequest request) {
        Long workingSessionId = request.getWorkingSessionId();

        WorkingSession workingSession = workingSessionRepository.findById(workingSessionId).orElseThrow(() ->
                new EntityNotFoundException(String.format("Working session with id=%d has not been found", workingSessionId)));

        User owner = workingSession.getOwner();
        if (!owner.getLogin().equals(request.getOwnerLogin())) {
            throw new EntityNotFoundException(String.format("Working session with id=%d has not been found", workingSessionId));
        }

        workingSessionRepository.deleteById(workingSessionId);
    }

    private WorkingSessionState requireValidWorkingSessionStateOrNull(String state) {
        if(state == null) return null;

        state = state.toUpperCase();

        Set<String> validStates = Arrays.stream(WorkingSessionState.values())
                .map(WorkingSessionState::name)
                .collect(Collectors.toSet());

        if (!validStates.contains(state)) {
            throw new WorkingSessionStateParsingException(String.format("State='%s' isn't valid working session state",
                    state));
        }

        return WorkingSessionState.valueOf(state);
    }

    private Specification<WorkingSession> getWorkingSessionsWithOwner(User user) {
        return (root, criteriaQuery, criteriaBuilder) ->
                criteriaBuilder.equal(root.get("owner"), user);
    }

    private Specification<WorkingSession> getWorkingSessionsWithState(WorkingSessionState state) {
        return (root, criteriaQuery, criteriaBuilder) ->
                criteriaBuilder.equal(root.get("state"), state);
    }

    private Specification<WorkingSession> getWorkingSessionsWithStartTime(LocalDateTime startTime) {
        return (root, criteriaQuery, criteriaBuilder) ->
                criteriaBuilder.equal(root.get("startTime"), startTime);
    }

    private WorkingSessionInfo map(WorkingSession workingSession) {
        List<UnitOfWorkInfo> unitsOfWork = workingSession.getUnitsOfWork()
                .stream()
                .map(unitOfWork -> UnitOfWorkInfo.builder()
                        .task(modelMapper.map(unitOfWork.getTask(), TaskInfo.class))
                        .startTime(unitOfWork.getStartTime())
                        .endTime(unitOfWork.getEndTime())
                        .build())
                .toList();

        return WorkingSessionInfo.builder()
                .workingSessionId(workingSession.getWorkingSessionId())
                .ownerLogin(workingSession.getOwner().getLogin())
                .specifiedDuration(workingSession.getSpecifiedDuration())
                .actualDuration(workingSession.getActualDuration())
                .state(workingSession.getState().name().toLowerCase())
                .startTime(workingSession.getStartTime())
                .unitsOfWork(unitsOfWork)
                .build();
    }
}
