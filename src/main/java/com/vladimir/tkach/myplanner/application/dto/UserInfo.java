package com.vladimir.tkach.myplanner.application.dto;

import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class UserInfo {
    private String login;
    private String firstName;

    @Builder
    public UserInfo(String login,
                    String firstName) {
        this.login = login;
        this.firstName = firstName;
    }
}
