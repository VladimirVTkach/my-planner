package com.vladimir.tkach.myplanner.controller.rest.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class UpdateUserControllerRequest {
    private String login;
    private String password;
    private String firstName;
}
