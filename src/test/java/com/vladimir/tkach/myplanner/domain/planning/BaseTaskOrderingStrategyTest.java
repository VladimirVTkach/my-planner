package com.vladimir.tkach.myplanner.domain.planning;

import com.vladimir.tkach.myplanner.domain.user.User;
import com.vladimir.tkach.myplanner.domain.user.Project;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class BaseTaskOrderingStrategyTest {
    @Test
    public void testOrderTasks_caseOne() {
        TaskOrderingStrategy taskOrderingStrategy = new BaseTaskOrderingStrategy();

        User user = new User("login",
                "password",
                "John");

        Project firstProject = Project.builder()
                .owner(user)
                .name("test_project_1")
                .goal("test_goal")
                .priority(1)
                .build();

        Project secondProject = Project.builder()
                .owner(user)
                .name("test_project_2")
                .goal("test_goal")
                .priority(2)
                .build();

        LocalDateTime now = LocalDateTime.now();

        Task firstTask = Task.builder()
                .project(firstProject)
                .title("test_task_1")
                .description("test_description")
                .deadline(now.plus(Duration.ofDays(5)))
                .priority(1)
                .timeForCompletion(Duration.ofMinutes(30))
                .timeForRest(Duration.ofMinutes(10))
                .build();

        Task secondTask = Task.builder()
                .project(firstProject)
                .title("test_task_2")
                .description("test_description")
                .deadline(now.plus(Duration.ofDays(5)))
                .priority(2)
                .timeForCompletion(Duration.ofMinutes(15))
                .timeForRest(Duration.ofMinutes(5))
                .build();

        Task thirdTask = Task.builder()
                .project(secondProject)
                .title("test_task_3")
                .description("test_description")
                .deadline(now.plus(Duration.ofHours(10)))
                .priority(4)
                .timeForCompletion(Duration.ofMinutes(40))
                .timeForRest(Duration.ofMinutes(15))
                .build();

        List<Task> tasks = new ArrayList<>(List.of(firstTask, secondTask, thirdTask));

        List<Task> expectedTaskOrder = List.of(thirdTask, firstTask, secondTask);

        Assertions.assertEquals(expectedTaskOrder, taskOrderingStrategy.orderTasks(tasks));
    }

    @Test
    public void testOrderTasks_caseTwo() {
        TaskOrderingStrategy taskOrderingStrategy = new BaseTaskOrderingStrategy();

        User user = new User("login",
                "password",
                "John");

        Project firstProject = Project.builder()
                .owner(user)
                .name("test_project_1")
                .goal("test_goal")
                .priority(1)
                .build();

        Project secondProject = Project.builder()
                .owner(user)
                .name("test_project_2")
                .goal("test_goal")
                .priority(2)
                .build();

        LocalDateTime now = LocalDateTime.now();

        Task firstTask = Task.builder()
                .project(firstProject)
                .title("test_task_1")
                .description("test_description")
                .deadline(now.plus(Duration.ofDays(5)))
                .priority(1)
                .timeForCompletion(Duration.ofMinutes(30))
                .timeForRest(Duration.ofMinutes(10))
                .build();

        Task secondTask = Task.builder()
                .project(firstProject)
                .title("test_task_2")
                .description("test_description")
                .deadline(now.plus(Duration.ofDays(5)))
                .priority(1)
                .timeForCompletion(Duration.ofMinutes(15))
                .timeForRest(Duration.ofMinutes(5))
                .build();

        Task thirdTask = Task.builder()
                .project(secondProject)
                .title("test_task_3")
                .description("test_description")
                .deadline(now.plus(Duration.ofHours(10)))
                .priority(4)
                .timeForCompletion(Duration.ofMinutes(40))
                .timeForRest(Duration.ofMinutes(15))
                .build();

        List<Task> tasks = new ArrayList<>(List.of(firstTask, secondTask, thirdTask));

        List<Task> expectedTaskOrder = List.of(thirdTask, secondTask, firstTask);

        Assertions.assertEquals(expectedTaskOrder, taskOrderingStrategy.orderTasks(tasks));
    }

    @Test
    public void testOrderTasks_caseThree() {
        TaskOrderingStrategy taskOrderingStrategy = new BaseTaskOrderingStrategy();

        User user = new User("login",
                "password",
                "John");

        Project firstProject = Project.builder()
                .owner(user)
                .name("test_project_1")
                .goal("test_goal")
                .priority(1)
                .build();

        Project secondProject = Project.builder()
                .owner(user)
                .name("test_project_2")
                .goal("test_goal")
                .priority(2)
                .build();

        LocalDateTime now = LocalDateTime.now();

        Task firstTask = Task.builder()
                .project(firstProject)
                .title("test_task_1")
                .description("test_description")
                .deadline(now.plus(Duration.ofDays(1)))
                .priority(1)
                .timeForCompletion(Duration.ofMinutes(30))
                .timeForRest(Duration.ofMinutes(10))
                .build();

        Task secondTask = Task.builder()
                .project(firstProject)
                .title("test_task_2")
                .description("test_description")
                .deadline(now.plus(Duration.ofDays(1)))
                .priority(3)
                .timeForCompletion(Duration.ofMinutes(15))
                .timeForRest(Duration.ofMinutes(5))
                .build();

        Task thirdTask = Task.builder()
                .project(secondProject)
                .title("test_task_3")
                .description("test_description")
                .deadline(now.plus(Duration.ofDays(1)))
                .priority(4)
                .timeForCompletion(Duration.ofMinutes(40))
                .timeForRest(Duration.ofMinutes(15))
                .build();

        List<Task> tasks = new ArrayList<>(List.of(firstTask, secondTask, thirdTask));

        List<Task> expectedTaskOrder = List.of(firstTask, secondTask, thirdTask);

        Assertions.assertEquals(expectedTaskOrder, taskOrderingStrategy.orderTasks(tasks));
    }
}
