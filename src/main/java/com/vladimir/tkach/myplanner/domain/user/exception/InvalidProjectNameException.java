package com.vladimir.tkach.myplanner.domain.user.exception;

public class InvalidProjectNameException extends UserException {
    public InvalidProjectNameException(String message) {
        super(message);
    }
}
