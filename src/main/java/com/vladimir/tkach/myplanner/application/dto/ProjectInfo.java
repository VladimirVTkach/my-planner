package com.vladimir.tkach.myplanner.application.dto;

import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@NoArgsConstructor
public class ProjectInfo {
    private Long projectId;
    private String name;
    private String goal;
    private Integer priority;
    private LocalDateTime creationTime;
    private LocalDateTime updateTime;
    private String ownerLogin;

    @Builder
    public ProjectInfo(Long projectId,
                       String name,
                       String goal,
                       Integer priority,
                       LocalDateTime creationTime,
                       LocalDateTime updateTime,
                       String ownerLogin) {
        this.projectId = projectId;
        this.name = name;
        this.goal = goal;
        this.priority = priority;
        this.creationTime = creationTime;
        this.updateTime = updateTime;
        this.ownerLogin = ownerLogin;
    }
}
