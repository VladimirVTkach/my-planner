package com.vladimir.tkach.myplanner.domain.planning.exception;

public class PlanningException extends RuntimeException {
    public PlanningException(String message) {
        super(message);
    }
}
