package com.vladimir.tkach.myplanner.controller.rest.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class GetUserProjectsControllerRequest {
    private String name;
    private Integer limit;
    private Integer offset;
}
