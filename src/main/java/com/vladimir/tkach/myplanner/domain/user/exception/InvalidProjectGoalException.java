package com.vladimir.tkach.myplanner.domain.user.exception;

public class InvalidProjectGoalException extends UserException {
    public InvalidProjectGoalException(String message) {
        super(message);
    }
}
