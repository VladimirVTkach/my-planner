package com.vladimir.tkach.myplanner.application.dto;

import lombok.Builder;
import lombok.Data;
import lombok.NonNull;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

@Data
@Builder
public class SplitTaskRequest {
    @NonNull
    private String userLogin;

    @NotNull(message = "task id can't be null")
    private long taskId;

    @Positive(message = "subtasks count must be positive")
    private int subtasksCount;
}
