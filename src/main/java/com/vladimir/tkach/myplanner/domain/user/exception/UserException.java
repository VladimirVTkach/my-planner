package com.vladimir.tkach.myplanner.domain.user.exception;

public class UserException extends RuntimeException {
    public UserException(String message) {
        super(message);
    }
}
