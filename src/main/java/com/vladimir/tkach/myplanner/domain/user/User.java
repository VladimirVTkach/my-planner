package com.vladimir.tkach.myplanner.domain.user;

import com.vladimir.tkach.myplanner.domain.user.exception.InvalidFirstNameException;
import com.vladimir.tkach.myplanner.domain.user.exception.InvalidUserLoginException;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

import javax.persistence.*;

@Data
@NoArgsConstructor
@Table(name = "user_account")
@Entity
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long userId;

    @NonNull
    private String login;

    @NonNull
    private String password;

    private String firstName;

    @Builder
    public User(@NonNull String login,
                @NonNull String password,
                @NonNull String firstName) {
        this.login = requireValidLogin(login);
        this.password = password;
        this.firstName = requireValidFirstName(firstName);
    }

    public void setLogin(@NonNull String login) {
        this.login = requireValidLogin(login);
    }

    public void setFirstName(@NonNull String firstName) {
        this.firstName = requireValidFirstName(firstName);
    }

    private String requireValidLogin(String login) {
        if (login.isBlank()) {
            throw new InvalidUserLoginException("login must be non-empty");
        }

        if (login.length() > 25) {
            throw new InvalidUserLoginException("max allowed login length is 25 characters");
        }

        return login;
    }

    private String requireValidFirstName(String firstName) {
        if (firstName.isBlank()) {
            throw new InvalidFirstNameException("first name must be non-empty");
        }

        if (firstName.length() > 30) {
            throw new InvalidFirstNameException("max allowed first name length is 30 characters");
        }

        return firstName;
    }
}
