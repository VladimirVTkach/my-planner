package com.vladimir.tkach.myplanner.application.dto;

import lombok.Builder;
import lombok.Data;
import lombok.NonNull;

import javax.validation.constraints.Positive;
import javax.validation.constraints.PositiveOrZero;
import java.time.LocalDateTime;
import java.util.Objects;

@Data
@Builder
public class GetAllTasksRequest {
    @NonNull
    private String userLogin;

    private Boolean completed;

    private LocalDateTime deadlineAfter;

    @Positive(message = "limit must be positive")
    private Integer limit;

    @PositiveOrZero(message = "offset must be non-negative")
    private Integer offset;

    public Boolean getCompleted() {
        return Objects.requireNonNullElse(completed, false);
    }

    public LocalDateTime getDeadlineAfter() {
        return Objects.requireNonNullElse(deadlineAfter, LocalDateTime.now());
    }

    public Integer getLimit() {
        return Objects.requireNonNullElse(limit, 10);
    }

    public Integer getOffset() {
        return Objects.requireNonNullElse(offset, 0);
    }
}
