package com.vladimir.tkach.myplanner.infrastructure.repository;

import com.vladimir.tkach.myplanner.domain.planning.Task;
import com.vladimir.tkach.myplanner.domain.user.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.time.LocalDateTime;
import java.util.List;

public interface TaskRepository extends JpaRepository<Task, Long>, JpaSpecificationExecutor<Task> {
    List<Task> findAllByProjectOwnerAndCompletedAndDeadlineAfter(User user,
                                                                 boolean completed,
                                                                 LocalDateTime currentDateTime);
}
