package com.vladimir.tkach.myplanner.application;

import com.vladimir.tkach.myplanner.application.dto.CreateUserRequest;
import com.vladimir.tkach.myplanner.application.dto.UpdateUserRequest;
import com.vladimir.tkach.myplanner.application.dto.UserInfo;
import com.vladimir.tkach.myplanner.application.exception.EntityNotFoundException;
import com.vladimir.tkach.myplanner.application.exception.SuchUserAlreadyExistsException;
import com.vladimir.tkach.myplanner.domain.user.User;
import com.vladimir.tkach.myplanner.infrastructure.repository.UserRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@SpringBootTest
public class UserServiceTest {

    @Autowired
    private UserService userService;

    @MockBean
    private UserRepository userRepository;

    @Test
    public void testCreateUser_whenSuchUserAlreadyExists() {
        User existingUser = User.builder()
                .login("test")
                .password("password")
                .firstName("first_name")
                .build();

        when(userRepository.findByLogin(existingUser.getLogin()))
                .thenReturn(Optional.of(existingUser));

        assertThrows(SuchUserAlreadyExistsException.class, () -> userService.createUser(CreateUserRequest.builder()
                .login("test")
                .password("password")
                .firstName("first_name")
                .build()));
    }

    @Test
    public void testCreateUser_whenSuchUserDoesNotExists() {
        String login = "test";
        String firstName = "first_name";

        when(userRepository.save(any()))
                .thenAnswer(invocationOnMock -> invocationOnMock.getArgument(0));

        UserInfo createdUser = userService.createUser(CreateUserRequest.builder()
                .login(login)
                .password("password")
                .firstName(firstName)
                .build());

        assertEquals(login, createdUser.getLogin());
        assertEquals(firstName, createdUser.getFirstName());
    }

    @Test
    public void testUpdateUser_whenUserWithPassedLoginDoesntExist() {
        assertThrows(EntityNotFoundException.class, () -> userService.updateUser(UpdateUserRequest.builder()
                .currentLogin("login")
                .newLogin("updated_login")
                .password("updated_password")
                .firstName("updated_first_name")
                .build()));
    }

    @Test
    public void testUpdateUser_whenUserWithPassedLoginExists() {
        User user = User.builder()
                .login("login")
                .password("password")
                .firstName("first_name")
                .build();

        when(userRepository.findByLogin(user.getLogin()))
                .thenReturn(Optional.of(user));

        when(userRepository.save(any()))
                .thenAnswer(invocationOnMock -> invocationOnMock.getArgument(0));

        String newLogin = "updated_login";
        String newFirstName = "updated_first_name";

        UserInfo updatedUser = userService.updateUser(UpdateUserRequest.builder()
                .currentLogin("login")
                .newLogin(newLogin)
                .password("password")
                .firstName(newFirstName)
                .build());

        assertEquals(newLogin, updatedUser.getLogin());
        assertEquals(newFirstName, updatedUser.getFirstName());
    }
}
