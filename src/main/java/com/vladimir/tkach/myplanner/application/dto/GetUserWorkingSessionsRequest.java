package com.vladimir.tkach.myplanner.application.dto;

import lombok.Builder;
import lombok.Data;
import lombok.NonNull;

import javax.validation.constraints.Positive;
import javax.validation.constraints.PositiveOrZero;
import java.time.LocalDateTime;

@Data
@Builder
public class GetUserWorkingSessionsRequest {
    @NonNull
    private String userLogin;

    private String state;

    private LocalDateTime startTime;

    @Positive(message = "limit must be positive")
    private Integer limit;

    @PositiveOrZero(message = "offset must be non-negative")
    private Integer offset;
}
