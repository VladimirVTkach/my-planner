package com.vladimir.tkach.myplanner.application.dto;

import lombok.Builder;
import lombok.Data;
import lombok.NonNull;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.PositiveOrZero;
import java.util.Objects;

@Data
@Builder
public class GetProjectTasksByTitleRequest {
    @NonNull
    private String userLogin;

    @NotNull(message = "project id can't be null")
    private Long projectId;

    private String title;

    @Positive(message = "limit must be positive")
    private Integer limit;

    @PositiveOrZero(message = "offset must be non-negative")
    private Integer offset;

    public Integer getLimit() {
        return Objects.requireNonNullElse(limit, 10);
    }

    public Integer getOffset() {
        return Objects.requireNonNullElse(offset, 0);
    }
}
